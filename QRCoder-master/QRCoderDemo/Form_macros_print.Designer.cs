﻿namespace QRCoderDemo
{
    partial class Form_macros_print
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_macros_print));
            this.label_title_input = new System.Windows.Forms.Label();
            this.label_title_output1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button_clear_data = new System.Windows.Forms.Button();
            this.button_gen_data = new System.Windows.Forms.Button();
            this.button_execute_print = new System.Windows.Forms.Button();
            this.label_textOutput = new System.Windows.Forms.Label();
            this.lable_dateTime_now = new System.Windows.Forms.Label();
            this.button_synchronize = new System.Windows.Forms.Button();
            this.title_dateTime = new System.Windows.Forms.Label();
            this.label_image_output1 = new System.Windows.Forms.Label();
            this.pictureBoxQRCode = new System.Windows.Forms.PictureBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label_submit_number = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_sync_number = new System.Windows.Forms.Label();
            this.checkBox_tools_show = new System.Windows.Forms.CheckBox();
            this.label_textQRCode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_error_number = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_state_connDB = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_state_prop = new System.Windows.Forms.Label();
            this.comboBox_levelQR = new System.Windows.Forms.ComboBox();
            this.numericUpDown_iconSize = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_macNo = new System.Windows.Forms.NumericUpDown();
            this.groupBox_config_panel = new System.Windows.Forms.GroupBox();
            this.textBox_get_sheetChr = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox_txt_validation_query = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.numericUpDown_maximum_weight_g = new System.Windows.Forms.NumericUpDown();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.numericUpDown_minimum_weight_g = new System.Windows.Forms.NumericUpDown();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox_logger_path = new System.Windows.Forms.TextBox();
            this.groupBox_setting_print = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox_pictureBox_size = new System.Windows.Forms.TextBox();
            this.textBox_set_post_size = new System.Windows.Forms.TextBox();
            this.textBox_set_post_x = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox_set_post_y = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.textBox_sync_sheetChr = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label_cache_table_log = new System.Windows.Forms.Label();
            this.label_OADate_now = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox_range_time = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox_text_readLine = new System.Windows.Forms.TextBox();
            this.textBox_xml_path = new System.Windows.Forms.TextBox();
            this.textBox_dbf_path = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown_interval_time = new System.Windows.Forms.NumericUpDown();
            this.textBox_iconPath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label_submit_lastTime = new System.Windows.Forms.Label();
            this.label_state_cloud = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label_sync_lastTime = new System.Windows.Forms.Label();
            this.eventLog1 = new System.Diagnostics.EventLog();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox_in_count = new System.Windows.Forms.GroupBox();
            this.groupBox_cloud_sync = new System.Windows.Forms.GroupBox();
            this.groupBox_dateTime_now = new System.Windows.Forms.GroupBox();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.groupBox_Error = new System.Windows.Forms.GroupBox();
            this.label_state_network = new System.Windows.Forms.Label();
            this.label_state_print = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label_range_weight_kg = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label_fix_validation = new System.Windows.Forms.Label();
            this.label_display_mode = new System.Windows.Forms.Label();
            this.label_display_detail = new System.Windows.Forms.Label();
            this.button_repair_data = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQRCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_iconSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_macNo)).BeginInit();
            this.groupBox_config_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_maximum_weight_g)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_minimum_weight_g)).BeginInit();
            this.groupBox_setting_print.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_interval_time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).BeginInit();
            this.groupBox_in_count.SuspendLayout();
            this.groupBox_cloud_sync.SuspendLayout();
            this.groupBox_dateTime_now.SuspendLayout();
            this.groupBox_Error.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_title_input
            // 
            this.label_title_input.AutoSize = true;
            this.label_title_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_title_input.Location = new System.Drawing.Point(17, 221);
            this.label_title_input.Name = "label_title_input";
            this.label_title_input.Size = new System.Drawing.Size(110, 20);
            this.label_title_input.TabIndex = 0;
            this.label_title_input.Text = "Input.macros :";
            // 
            // label_title_output1
            // 
            this.label_title_output1.AutoSize = true;
            this.label_title_output1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_title_output1.Location = new System.Drawing.Point(34, 356);
            this.label_title_output1.Name = "label_title_output1";
            this.label_title_output1.Size = new System.Drawing.Size(93, 20);
            this.label_title_output1.TabIndex = 1;
            this.label_title_output1.Text = "text output :";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(79, 95);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(380, 15);
            this.progressBar1.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(131, 223);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(152, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(131, 249);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(152, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyDown);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(131, 275);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(152, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            this.textBox3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox3_KeyDown);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(131, 301);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(152, 20);
            this.textBox4.TabIndex = 3;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox4_KeyDown);
            // 
            // button_clear_data
            // 
            this.button_clear_data.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button_clear_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button_clear_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button_clear_data.Location = new System.Drawing.Point(474, 114);
            this.button_clear_data.Name = "button_clear_data";
            this.button_clear_data.Size = new System.Drawing.Size(56, 26);
            this.button_clear_data.TabIndex = 6;
            this.button_clear_data.Text = "clear";
            this.button_clear_data.UseVisualStyleBackColor = false;
            this.button_clear_data.Visible = false;
            this.button_clear_data.Click += new System.EventHandler(this.button_clear_data_Click);
            // 
            // button_gen_data
            // 
            this.button_gen_data.BackColor = System.Drawing.Color.Yellow;
            this.button_gen_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button_gen_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button_gen_data.Location = new System.Drawing.Point(531, 114);
            this.button_gen_data.Name = "button_gen_data";
            this.button_gen_data.Size = new System.Drawing.Size(56, 26);
            this.button_gen_data.TabIndex = 7;
            this.button_gen_data.Text = "gen";
            this.button_gen_data.UseVisualStyleBackColor = false;
            this.button_gen_data.Visible = false;
            this.button_gen_data.Click += new System.EventHandler(this.button_gen_data_Click);
            // 
            // button_execute_print
            // 
            this.button_execute_print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_execute_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button_execute_print.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button_execute_print.Location = new System.Drawing.Point(588, 114);
            this.button_execute_print.Name = "button_execute_print";
            this.button_execute_print.Size = new System.Drawing.Size(56, 26);
            this.button_execute_print.TabIndex = 8;
            this.button_execute_print.Text = "print";
            this.button_execute_print.UseVisualStyleBackColor = false;
            this.button_execute_print.Visible = false;
            this.button_execute_print.Click += new System.EventHandler(this.button_execute_print_Click);
            // 
            // label_textOutput
            // 
            this.label_textOutput.AutoSize = true;
            this.label_textOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_textOutput.Location = new System.Drawing.Point(128, 359);
            this.label_textOutput.Name = "label_textOutput";
            this.label_textOutput.Size = new System.Drawing.Size(34, 15);
            this.label_textOutput.TabIndex = 10;
            this.label_textOutput.Text = ".........";
            // 
            // lable_dateTime_now
            // 
            this.lable_dateTime_now.AutoSize = true;
            this.lable_dateTime_now.BackColor = System.Drawing.Color.Lime;
            this.lable_dateTime_now.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lable_dateTime_now.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lable_dateTime_now.Location = new System.Drawing.Point(10, 47);
            this.lable_dateTime_now.Name = "lable_dateTime_now";
            this.lable_dateTime_now.Size = new System.Drawing.Size(146, 16);
            this.lable_dateTime_now.TabIndex = 11;
            this.lable_dateTime_now.Text = "MM/dd/yyyy HH:mm:ss";
            // 
            // button_synchronize
            // 
            this.button_synchronize.BackColor = System.Drawing.Color.Fuchsia;
            this.button_synchronize.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button_synchronize.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button_synchronize.Location = new System.Drawing.Point(645, 114);
            this.button_synchronize.Name = "button_synchronize";
            this.button_synchronize.Size = new System.Drawing.Size(56, 26);
            this.button_synchronize.TabIndex = 9;
            this.button_synchronize.Text = "sync";
            this.button_synchronize.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button_synchronize.UseVisualStyleBackColor = false;
            this.button_synchronize.Visible = false;
            this.button_synchronize.Click += new System.EventHandler(this.button_synchronize_Click);
            // 
            // title_dateTime
            // 
            this.title_dateTime.AutoSize = true;
            this.title_dateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.title_dateTime.ForeColor = System.Drawing.Color.Gray;
            this.title_dateTime.Location = new System.Drawing.Point(10, 16);
            this.title_dateTime.Name = "title_dateTime";
            this.title_dateTime.Size = new System.Drawing.Size(120, 26);
            this.title_dateTime.TabIndex = 13;
            this.title_dateTime.Text = "(MM/dd/yyyy hh:mm:ss)\r\nClock now";
            // 
            // label_image_output1
            // 
            this.label_image_output1.AutoSize = true;
            this.label_image_output1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_image_output1.Location = new System.Drawing.Point(17, 448);
            this.label_image_output1.Name = "label_image_output1";
            this.label_image_output1.Size = new System.Drawing.Size(110, 20);
            this.label_image_output1.TabIndex = 15;
            this.label_image_output1.Text = "image output :";
            // 
            // pictureBoxQRCode
            // 
            this.pictureBoxQRCode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBoxQRCode.Enabled = false;
            this.pictureBoxQRCode.Location = new System.Drawing.Point(131, 450);
            this.pictureBoxQRCode.Name = "pictureBoxQRCode";
            this.pictureBoxQRCode.Size = new System.Drawing.Size(146, 146);
            this.pictureBoxQRCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxQRCode.TabIndex = 16;
            this.pictureBoxQRCode.TabStop = false;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(131, 327);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(152, 20);
            this.textBox5.TabIndex = 4;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox5_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(9, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "in count";
            // 
            // label_submit_number
            // 
            this.label_submit_number.AutoSize = true;
            this.label_submit_number.BackColor = System.Drawing.Color.Yellow;
            this.label_submit_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_submit_number.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_submit_number.Location = new System.Drawing.Point(9, 48);
            this.label_submit_number.Name = "label_submit_number";
            this.label_submit_number.Size = new System.Drawing.Size(14, 15);
            this.label_submit_number.TabIndex = 18;
            this.label_submit_number.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(6, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "cloud sync";
            // 
            // label_sync_number
            // 
            this.label_sync_number.AutoSize = true;
            this.label_sync_number.BackColor = System.Drawing.Color.Fuchsia;
            this.label_sync_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_sync_number.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_sync_number.Location = new System.Drawing.Point(6, 49);
            this.label_sync_number.Name = "label_sync_number";
            this.label_sync_number.Size = new System.Drawing.Size(14, 15);
            this.label_sync_number.TabIndex = 20;
            this.label_sync_number.Text = "0";
            // 
            // checkBox_tools_show
            // 
            this.checkBox_tools_show.AutoSize = true;
            this.checkBox_tools_show.Location = new System.Drawing.Point(625, 94);
            this.checkBox_tools_show.Name = "checkBox_tools_show";
            this.checkBox_tools_show.Size = new System.Drawing.Size(76, 17);
            this.checkBox_tools_show.TabIndex = 5;
            this.checkBox_tools_show.Text = "show tools";
            this.checkBox_tools_show.UseVisualStyleBackColor = true;
            this.checkBox_tools_show.CheckedChanged += new System.EventHandler(this.checkBox_show_CheckStateChanged);
            this.checkBox_tools_show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkBox_show_KeyDown);
            // 
            // label_textQRCode
            // 
            this.label_textQRCode.AutoSize = true;
            this.label_textQRCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_textQRCode.Location = new System.Drawing.Point(19, 663);
            this.label_textQRCode.Name = "label_textQRCode";
            this.label_textQRCode.Size = new System.Drawing.Size(25, 15);
            this.label_textQRCode.TabIndex = 190;
            this.label_textQRCode.Text = "......";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(11, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "in Error";
            // 
            // label_error_number
            // 
            this.label_error_number.AutoSize = true;
            this.label_error_number.BackColor = System.Drawing.Color.Red;
            this.label_error_number.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_error_number.ForeColor = System.Drawing.Color.White;
            this.label_error_number.Location = new System.Drawing.Point(11, 47);
            this.label_error_number.Name = "label_error_number";
            this.label_error_number.Size = new System.Drawing.Size(14, 15);
            this.label_error_number.TabIndex = 24;
            this.label_error_number.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(27, 144);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "state db :";
            // 
            // label_state_connDB
            // 
            this.label_state_connDB.AutoSize = true;
            this.label_state_connDB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label_state_connDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_state_connDB.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_state_connDB.Location = new System.Drawing.Point(78, 144);
            this.label_state_connDB.Name = "label_state_connDB";
            this.label_state_connDB.Size = new System.Drawing.Size(19, 13);
            this.label_state_connDB.TabIndex = 27;
            this.label_state_connDB.Text = "....";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(24, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "progress :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label6.ForeColor = System.Drawing.Color.Gray;
            this.label6.Location = new System.Drawing.Point(27, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "prop db :";
            // 
            // label_state_prop
            // 
            this.label_state_prop.AutoSize = true;
            this.label_state_prop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label_state_prop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_state_prop.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_state_prop.Location = new System.Drawing.Point(78, 168);
            this.label_state_prop.Name = "label_state_prop";
            this.label_state_prop.Size = new System.Drawing.Size(19, 13);
            this.label_state_prop.TabIndex = 30;
            this.label_state_prop.Text = "....";
            // 
            // comboBox_levelQR
            // 
            this.comboBox_levelQR.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.comboBox_levelQR.FormattingEnabled = true;
            this.comboBox_levelQR.Items.AddRange(new object[] {
            "L",
            "M",
            "Q",
            "H"});
            this.comboBox_levelQR.Location = new System.Drawing.Point(102, 38);
            this.comboBox_levelQR.Name = "comboBox_levelQR";
            this.comboBox_levelQR.Size = new System.Drawing.Size(56, 21);
            this.comboBox_levelQR.TabIndex = 11;
            // 
            // numericUpDown_iconSize
            // 
            this.numericUpDown_iconSize.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.numericUpDown_iconSize.Location = new System.Drawing.Point(102, 83);
            this.numericUpDown_iconSize.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown_iconSize.Name = "numericUpDown_iconSize";
            this.numericUpDown_iconSize.Size = new System.Drawing.Size(56, 20);
            this.numericUpDown_iconSize.TabIndex = 13;
            this.numericUpDown_iconSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_iconSize.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // numericUpDown_macNo
            // 
            this.numericUpDown_macNo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.numericUpDown_macNo.Location = new System.Drawing.Point(102, 105);
            this.numericUpDown_macNo.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDown_macNo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_macNo.Name = "numericUpDown_macNo";
            this.numericUpDown_macNo.Size = new System.Drawing.Size(56, 20);
            this.numericUpDown_macNo.TabIndex = 14;
            this.numericUpDown_macNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_macNo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox_config_panel
            // 
            this.groupBox_config_panel.Controls.Add(this.textBox_get_sheetChr);
            this.groupBox_config_panel.Controls.Add(this.label48);
            this.groupBox_config_panel.Controls.Add(this.textBox_txt_validation_query);
            this.groupBox_config_panel.Controls.Add(this.label47);
            this.groupBox_config_panel.Controls.Add(this.numericUpDown_maximum_weight_g);
            this.groupBox_config_panel.Controls.Add(this.label44);
            this.groupBox_config_panel.Controls.Add(this.label45);
            this.groupBox_config_panel.Controls.Add(this.numericUpDown_minimum_weight_g);
            this.groupBox_config_panel.Controls.Add(this.label43);
            this.groupBox_config_panel.Controls.Add(this.label42);
            this.groupBox_config_panel.Controls.Add(this.label40);
            this.groupBox_config_panel.Controls.Add(this.label39);
            this.groupBox_config_panel.Controls.Add(this.textBox_logger_path);
            this.groupBox_config_panel.Controls.Add(this.groupBox_setting_print);
            this.groupBox_config_panel.Controls.Add(this.label29);
            this.groupBox_config_panel.Controls.Add(this.label28);
            this.groupBox_config_panel.Controls.Add(this.textBox_sync_sheetChr);
            this.groupBox_config_panel.Controls.Add(this.label27);
            this.groupBox_config_panel.Controls.Add(this.label_cache_table_log);
            this.groupBox_config_panel.Controls.Add(this.label_OADate_now);
            this.groupBox_config_panel.Controls.Add(this.label25);
            this.groupBox_config_panel.Controls.Add(this.label23);
            this.groupBox_config_panel.Controls.Add(this.textBox_range_time);
            this.groupBox_config_panel.Controls.Add(this.label22);
            this.groupBox_config_panel.Controls.Add(this.label21);
            this.groupBox_config_panel.Controls.Add(this.label20);
            this.groupBox_config_panel.Controls.Add(this.textBox_text_readLine);
            this.groupBox_config_panel.Controls.Add(this.textBox_xml_path);
            this.groupBox_config_panel.Controls.Add(this.textBox_dbf_path);
            this.groupBox_config_panel.Controls.Add(this.label18);
            this.groupBox_config_panel.Controls.Add(this.label17);
            this.groupBox_config_panel.Controls.Add(this.label16);
            this.groupBox_config_panel.Controls.Add(this.label15);
            this.groupBox_config_panel.Controls.Add(this.label14);
            this.groupBox_config_panel.Controls.Add(this.label13);
            this.groupBox_config_panel.Controls.Add(this.textBox11);
            this.groupBox_config_panel.Controls.Add(this.comboBox1);
            this.groupBox_config_panel.Controls.Add(this.textBox10);
            this.groupBox_config_panel.Controls.Add(this.textBox9);
            this.groupBox_config_panel.Controls.Add(this.textBox8);
            this.groupBox_config_panel.Controls.Add(this.textBox7);
            this.groupBox_config_panel.Controls.Add(this.textBox6);
            this.groupBox_config_panel.Controls.Add(this.label12);
            this.groupBox_config_panel.Controls.Add(this.numericUpDown_interval_time);
            this.groupBox_config_panel.Controls.Add(this.textBox_iconPath);
            this.groupBox_config_panel.Controls.Add(this.label11);
            this.groupBox_config_panel.Controls.Add(this.label10);
            this.groupBox_config_panel.Controls.Add(this.label9);
            this.groupBox_config_panel.Controls.Add(this.label8);
            this.groupBox_config_panel.Controls.Add(this.label7);
            this.groupBox_config_panel.Controls.Add(this.numericUpDown_macNo);
            this.groupBox_config_panel.Controls.Add(this.numericUpDown_iconSize);
            this.groupBox_config_panel.Controls.Add(this.comboBox_levelQR);
            this.groupBox_config_panel.Location = new System.Drawing.Point(301, 144);
            this.groupBox_config_panel.Name = "groupBox_config_panel";
            this.groupBox_config_panel.Size = new System.Drawing.Size(404, 512);
            this.groupBox_config_panel.TabIndex = 31;
            this.groupBox_config_panel.TabStop = false;
            this.groupBox_config_panel.Text = "configuration panel";
            this.groupBox_config_panel.Visible = false;
            // 
            // textBox_get_sheetChr
            // 
            this.textBox_get_sheetChr.Location = new System.Drawing.Point(265, 270);
            this.textBox_get_sheetChr.Name = "textBox_get_sheetChr";
            this.textBox_get_sheetChr.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_get_sheetChr.Size = new System.Drawing.Size(99, 20);
            this.textBox_get_sheetChr.TabIndex = 80;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label48.ForeColor = System.Drawing.Color.Gray;
            this.label48.Location = new System.Drawing.Point(286, 337);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(104, 12);
            this.label48.TabIndex = 79;
            this.label48.Text = "(find_word_text,char_n)";
            // 
            // textBox_txt_validation_query
            // 
            this.textBox_txt_validation_query.Location = new System.Drawing.Point(301, 355);
            this.textBox_txt_validation_query.Name = "textBox_txt_validation_query";
            this.textBox_txt_validation_query.Size = new System.Drawing.Size(88, 20);
            this.textBox_txt_validation_query.TabIndex = 79;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label47.ForeColor = System.Drawing.Color.Gray;
            this.label47.Location = new System.Drawing.Point(204, 358);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(95, 13);
            this.label47.TabIndex = 78;
            this.label47.Text = "validation txt query";
            // 
            // numericUpDown_maximum_weight_g
            // 
            this.numericUpDown_maximum_weight_g.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.numericUpDown_maximum_weight_g.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown_maximum_weight_g.Location = new System.Drawing.Point(102, 311);
            this.numericUpDown_maximum_weight_g.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
            this.numericUpDown_maximum_weight_g.Name = "numericUpDown_maximum_weight_g";
            this.numericUpDown_maximum_weight_g.Size = new System.Drawing.Size(68, 20);
            this.numericUpDown_maximum_weight_g.TabIndex = 77;
            this.numericUpDown_maximum_weight_g.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label44.ForeColor = System.Drawing.Color.Gray;
            this.label44.Location = new System.Drawing.Point(175, 314);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(47, 12);
            this.label44.TabIndex = 76;
            this.label44.Text = "1000000 g";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(11, 313);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(85, 13);
            this.label45.TabIndex = 75;
            this.label45.Text = "maxi weight print";
            // 
            // numericUpDown_minimum_weight_g
            // 
            this.numericUpDown_minimum_weight_g.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.numericUpDown_minimum_weight_g.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown_minimum_weight_g.Location = new System.Drawing.Point(102, 334);
            this.numericUpDown_minimum_weight_g.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
            this.numericUpDown_minimum_weight_g.Name = "numericUpDown_minimum_weight_g";
            this.numericUpDown_minimum_weight_g.Size = new System.Drawing.Size(68, 20);
            this.numericUpDown_minimum_weight_g.TabIndex = 74;
            this.numericUpDown_minimum_weight_g.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Silver;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(101, 297);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(97, 12);
            this.label43.TabIndex = 73;
            this.label43.Text = "celling_floor_weight_g";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label42.ForeColor = System.Drawing.Color.Gray;
            this.label42.Location = new System.Drawing.Point(177, 337);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(32, 12);
            this.label42.TabIndex = 72;
            this.label42.Text = "1000 g";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(15, 336);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(82, 13);
            this.label40.TabIndex = 71;
            this.label40.Text = "mini weight print";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(7, 206);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(92, 13);
            this.label39.TabIndex = 69;
            this.label39.Text = "macro.logger.path";
            // 
            // textBox_logger_path
            // 
            this.textBox_logger_path.Location = new System.Drawing.Point(103, 203);
            this.textBox_logger_path.Name = "textBox_logger_path";
            this.textBox_logger_path.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_logger_path.Size = new System.Drawing.Size(99, 20);
            this.textBox_logger_path.TabIndex = 18;
            // 
            // groupBox_setting_print
            // 
            this.groupBox_setting_print.Controls.Add(this.label38);
            this.groupBox_setting_print.Controls.Add(this.label37);
            this.groupBox_setting_print.Controls.Add(this.label36);
            this.groupBox_setting_print.Controls.Add(this.label35);
            this.groupBox_setting_print.Controls.Add(this.textBox_pictureBox_size);
            this.groupBox_setting_print.Controls.Add(this.textBox_set_post_size);
            this.groupBox_setting_print.Controls.Add(this.textBox_set_post_x);
            this.groupBox_setting_print.Controls.Add(this.label34);
            this.groupBox_setting_print.Controls.Add(this.textBox_set_post_y);
            this.groupBox_setting_print.Controls.Add(this.label30);
            this.groupBox_setting_print.Controls.Add(this.label33);
            this.groupBox_setting_print.Controls.Add(this.label32);
            this.groupBox_setting_print.Location = new System.Drawing.Point(198, 379);
            this.groupBox_setting_print.Name = "groupBox_setting_print";
            this.groupBox_setting_print.Size = new System.Drawing.Size(200, 126);
            this.groupBox_setting_print.TabIndex = 67;
            this.groupBox_setting_print.TabStop = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label38.ForeColor = System.Drawing.Color.Gray;
            this.label38.Location = new System.Drawing.Point(156, 92);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(10, 24);
            this.label38.TabIndex = 70;
            this.label38.Text = "6\r\n7";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label37.ForeColor = System.Drawing.Color.Gray;
            this.label37.Location = new System.Drawing.Point(156, 65);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(20, 24);
            this.label37.TabIndex = 69;
            this.label37.Text = "132\r\n142";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label36.ForeColor = System.Drawing.Color.Gray;
            this.label36.Location = new System.Drawing.Point(153, 45);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 12);
            this.label36.TabIndex = 68;
            this.label36.Text = "9 only";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label35.ForeColor = System.Drawing.Color.Gray;
            this.label35.Location = new System.Drawing.Point(153, 20);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(39, 24);
            this.label35.TabIndex = 67;
            this.label35.Text = "136*136\r\n146*146";
            // 
            // textBox_pictureBox_size
            // 
            this.textBox_pictureBox_size.Location = new System.Drawing.Point(102, 17);
            this.textBox_pictureBox_size.Name = "textBox_pictureBox_size";
            this.textBox_pictureBox_size.Size = new System.Drawing.Size(48, 20);
            this.textBox_pictureBox_size.TabIndex = 20;
            this.textBox_pictureBox_size.Text = "146*146";
            // 
            // textBox_set_post_size
            // 
            this.textBox_set_post_size.Location = new System.Drawing.Point(102, 89);
            this.textBox_set_post_size.Name = "textBox_set_post_size";
            this.textBox_set_post_size.Size = new System.Drawing.Size(48, 20);
            this.textBox_set_post_size.TabIndex = 66;
            this.textBox_set_post_size.Text = "7";
            // 
            // textBox_set_post_x
            // 
            this.textBox_set_post_x.Location = new System.Drawing.Point(102, 41);
            this.textBox_set_post_x.Name = "textBox_set_post_x";
            this.textBox_set_post_x.Size = new System.Drawing.Size(48, 20);
            this.textBox_set_post_x.TabIndex = 20;
            this.textBox_set_post_x.Text = "9";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label34.ForeColor = System.Drawing.Color.Gray;
            this.label34.Location = new System.Drawing.Point(19, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(80, 13);
            this.label34.TabIndex = 65;
            this.label34.Text = "picture box size";
            // 
            // textBox_set_post_y
            // 
            this.textBox_set_post_y.Location = new System.Drawing.Point(103, 65);
            this.textBox_set_post_y.Name = "textBox_set_post_y";
            this.textBox_set_post_y.Size = new System.Drawing.Size(47, 20);
            this.textBox_set_post_y.TabIndex = 20;
            this.textBox_set_post_y.Text = "142";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label30.ForeColor = System.Drawing.Color.Gray;
            this.label30.Location = new System.Drawing.Point(22, 44);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 13);
            this.label30.TabIndex = 61;
            this.label30.Text = "print text post x";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label33.ForeColor = System.Drawing.Color.Gray;
            this.label33.Location = new System.Drawing.Point(31, 92);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 13);
            this.label33.TabIndex = 63;
            this.label33.Text = "print text size";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label32.ForeColor = System.Drawing.Color.Gray;
            this.label32.Location = new System.Drawing.Point(21, 68);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(78, 13);
            this.label32.TabIndex = 62;
            this.label32.Text = "print text post y";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label29.ForeColor = System.Drawing.Color.Gray;
            this.label29.Location = new System.Drawing.Point(207, 274);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(117, 24);
            this.label29.TabIndex = 58;
            this.label29.Text = "\"File_Id(Sheet_Name!A2:),\r\nFile_Id(\"Sheet_Name!A3:\")";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(26, 273);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(70, 13);
            this.label28.TabIndex = 57;
            this.label28.Text = "sheet of sync";
            // 
            // textBox_sync_sheetChr
            // 
            this.textBox_sync_sheetChr.Location = new System.Drawing.Point(102, 270);
            this.textBox_sync_sheetChr.Name = "textBox_sync_sheetChr";
            this.textBox_sync_sheetChr.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_sync_sheetChr.Size = new System.Drawing.Size(99, 20);
            this.textBox_sync_sheetChr.TabIndex = 20;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label27.ForeColor = System.Drawing.Color.Gray;
            this.label27.Location = new System.Drawing.Point(25, 231);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(74, 13);
            this.label27.TabIndex = 41;
            this.label27.Text = "routine state : ";
            // 
            // label_cache_table_log
            // 
            this.label_cache_table_log.AutoSize = true;
            this.label_cache_table_log.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label_cache_table_log.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_cache_table_log.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_cache_table_log.Location = new System.Drawing.Point(212, 20);
            this.label_cache_table_log.Name = "label_cache_table_log";
            this.label_cache_table_log.Size = new System.Drawing.Size(17, 12);
            this.label_cache_table_log.TabIndex = 55;
            this.label_cache_table_log.Text = "....";
            // 
            // label_OADate_now
            // 
            this.label_OADate_now.AutoSize = true;
            this.label_OADate_now.BackColor = System.Drawing.Color.Silver;
            this.label_OADate_now.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_OADate_now.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_OADate_now.Location = new System.Drawing.Point(100, 231);
            this.label_OADate_now.Name = "label_OADate_now";
            this.label_OADate_now.Size = new System.Drawing.Size(105, 12);
            this.label_OADate_now.TabIndex = 36;
            this.label_OADate_now.Text = "mm/dd/yyyy HH:mm:ss";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label25.ForeColor = System.Drawing.Color.Gray;
            this.label25.Location = new System.Drawing.Point(207, 250);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(112, 12);
            this.label25.TabIndex = 54;
            this.label25.Text = "\"20:00-20:02,19:01-19:50\"";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(15, 250);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 13);
            this.label23.TabIndex = 53;
            this.label23.Text = "range time sync";
            // 
            // textBox_range_time
            // 
            this.textBox_range_time.Location = new System.Drawing.Point(102, 247);
            this.textBox_range_time.Name = "textBox_range_time";
            this.textBox_range_time.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_range_time.Size = new System.Drawing.Size(99, 20);
            this.textBox_range_time.TabIndex = 19;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(24, 137);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 51;
            this.label22.Text = "readLine_path";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(36, 160);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 50;
            this.label21.Text = "xml.db.path";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(36, 183);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 49;
            this.label20.Text = "dbf.db.path";
            // 
            // textBox_text_readLine
            // 
            this.textBox_text_readLine.Location = new System.Drawing.Point(103, 134);
            this.textBox_text_readLine.Name = "textBox_text_readLine";
            this.textBox_text_readLine.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_text_readLine.Size = new System.Drawing.Size(99, 20);
            this.textBox_text_readLine.TabIndex = 15;
            // 
            // textBox_xml_path
            // 
            this.textBox_xml_path.Location = new System.Drawing.Point(103, 157);
            this.textBox_xml_path.Name = "textBox_xml_path";
            this.textBox_xml_path.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_xml_path.Size = new System.Drawing.Size(99, 20);
            this.textBox_xml_path.TabIndex = 16;
            // 
            // textBox_dbf_path
            // 
            this.textBox_dbf_path.Location = new System.Drawing.Point(103, 180);
            this.textBox_dbf_path.Name = "textBox_dbf_path";
            this.textBox_dbf_path.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_dbf_path.Size = new System.Drawing.Size(99, 20);
            this.textBox_dbf_path.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label18.ForeColor = System.Drawing.Color.Gray;
            this.label18.Location = new System.Drawing.Point(21, 486);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 12);
            this.label18.TabIndex = 45;
            this.label18.Text = "(DT)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label17.ForeColor = System.Drawing.Color.Gray;
            this.label17.Location = new System.Drawing.Point(21, 465);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 12);
            this.label17.TabIndex = 44;
            this.label17.Text = "(8008)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.ForeColor = System.Drawing.Color.Gray;
            this.label16.Location = new System.Drawing.Point(21, 444);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 12);
            this.label16.TabIndex = 43;
            this.label16.Text = "(240)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label15.ForeColor = System.Drawing.Color.Gray;
            this.label15.Location = new System.Drawing.Point(21, 423);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 12);
            this.label15.TabIndex = 42;
            this.label15.Text = "(01)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label14.ForeColor = System.Drawing.Color.Gray;
            this.label14.Location = new System.Drawing.Point(21, 402);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 12);
            this.label14.TabIndex = 41;
            this.label14.Text = "(21)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.ForeColor = System.Drawing.Color.Gray;
            this.label13.Location = new System.Drawing.Point(21, 379);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 12);
            this.label13.TabIndex = 40;
            this.label13.Text = "(02)";
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox11.Location = new System.Drawing.Point(72, 483);
            this.textBox11.Name = "textBox11";
            this.textBox11.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox11.Size = new System.Drawing.Size(120, 18);
            this.textBox11.TabIndex = 20;
            this.textBox11.Text = "dd/MM/yyyy HH:mm:ss";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "yyyyMMddHHmmss",
            "direct"});
            this.comboBox1.Location = new System.Drawing.Point(72, 376);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(120, 20);
            this.comboBox1.TabIndex = 20;
            this.comboBox1.Text = "yyyyMMddHHmmss";
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox10.Location = new System.Drawing.Point(72, 462);
            this.textBox10.Name = "textBox10";
            this.textBox10.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox10.Size = new System.Drawing.Size(120, 18);
            this.textBox10.TabIndex = 20;
            this.textBox10.Text = "yyMMddHHmmss";
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox9.Location = new System.Drawing.Point(72, 441);
            this.textBox9.Name = "textBox9";
            this.textBox9.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox9.Size = new System.Drawing.Size(120, 18);
            this.textBox9.TabIndex = 20;
            this.textBox9.Text = "WEIGHTSCALE";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox8.Location = new System.Drawing.Point(72, 420);
            this.textBox8.Name = "textBox8";
            this.textBox8.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox8.Size = new System.Drawing.Size(120, 18);
            this.textBox8.TabIndex = 20;
            this.textBox8.Text = "PACKAGE";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox7.Location = new System.Drawing.Point(72, 399);
            this.textBox7.Name = "textBox7";
            this.textBox7.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox7.Size = new System.Drawing.Size(120, 18);
            this.textBox7.TabIndex = 20;
            this.textBox7.Text = "8859229399998";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox6.Location = new System.Drawing.Point(43, 376);
            this.textBox6.Name = "textBox6";
            this.textBox6.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox6.Size = new System.Drawing.Size(25, 18);
            this.textBox6.TabIndex = 20;
            this.textBox6.Text = "PW";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.ForeColor = System.Drawing.Color.Gray;
            this.label12.Location = new System.Drawing.Point(22, 358);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(170, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "unchecked show tools for save.....";
            // 
            // numericUpDown_interval_time
            // 
            this.numericUpDown_interval_time.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.numericUpDown_interval_time.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDown_interval_time.Location = new System.Drawing.Point(102, 61);
            this.numericUpDown_interval_time.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericUpDown_interval_time.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_interval_time.Name = "numericUpDown_interval_time";
            this.numericUpDown_interval_time.Size = new System.Drawing.Size(56, 20);
            this.numericUpDown_interval_time.TabIndex = 12;
            this.numericUpDown_interval_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_interval_time.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // textBox_iconPath
            // 
            this.textBox_iconPath.Location = new System.Drawing.Point(102, 16);
            this.textBox_iconPath.Name = "textBox_iconPath";
            this.textBox_iconPath.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.textBox_iconPath.Size = new System.Drawing.Size(99, 20);
            this.textBox_iconPath.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(46, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "level.QR";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(45, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "size.Icon";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(45, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "icon.path";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(49, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "rMacNo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "in.milisec";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label19.ForeColor = System.Drawing.Color.Gray;
            this.label19.Location = new System.Drawing.Point(60, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(108, 26);
            this.label19.TabIndex = 32;
            this.label19.Text = "data last time\r\nMM/dd/yy HH:mm:ss";
            // 
            // label_submit_lastTime
            // 
            this.label_submit_lastTime.AutoSize = true;
            this.label_submit_lastTime.BackColor = System.Drawing.Color.Yellow;
            this.label_submit_lastTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_submit_lastTime.ForeColor = System.Drawing.Color.Black;
            this.label_submit_lastTime.Location = new System.Drawing.Point(60, 48);
            this.label_submit_lastTime.Name = "label_submit_lastTime";
            this.label_submit_lastTime.Size = new System.Drawing.Size(120, 15);
            this.label_submit_lastTime.TabIndex = 33;
            this.label_submit_lastTime.Text = "MM/dd/yy HH:mm:ss";
            // 
            // label_state_cloud
            // 
            this.label_state_cloud.AutoSize = true;
            this.label_state_cloud.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label_state_cloud.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_state_cloud.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_state_cloud.Location = new System.Drawing.Point(78, 118);
            this.label_state_cloud.Name = "label_state_cloud";
            this.label_state_cloud.Size = new System.Drawing.Size(19, 13);
            this.label_state_cloud.TabIndex = 35;
            this.label_state_cloud.Text = "....";
            this.label_state_cloud.Click += new System.EventHandler(this.label_state_cloud_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label24.ForeColor = System.Drawing.Color.Gray;
            this.label24.Location = new System.Drawing.Point(13, 118);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 13);
            this.label24.TabIndex = 34;
            this.label24.Text = "state cloud :";
            // 
            // label_sync_lastTime
            // 
            this.label_sync_lastTime.AutoSize = true;
            this.label_sync_lastTime.BackColor = System.Drawing.Color.Fuchsia;
            this.label_sync_lastTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_sync_lastTime.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_sync_lastTime.Location = new System.Drawing.Point(64, 49);
            this.label_sync_lastTime.Name = "label_sync_lastTime";
            this.label_sync_lastTime.Size = new System.Drawing.Size(146, 16);
            this.label_sync_lastTime.TabIndex = 37;
            this.label_sync_lastTime.Text = "mm/dd/yyyy HH:mm:ss";
            // 
            // eventLog1
            // 
            this.eventLog1.SynchronizingObject = this;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label26.ForeColor = System.Drawing.Color.Gray;
            this.label26.Location = new System.Drawing.Point(64, 14);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(108, 26);
            this.label26.TabIndex = 38;
            this.label26.Text = "sync last time\r\nMM/dd/yy HH:mm:ss";
            // 
            // groupBox_in_count
            // 
            this.groupBox_in_count.Controls.Add(this.label19);
            this.groupBox_in_count.Controls.Add(this.label_submit_number);
            this.groupBox_in_count.Controls.Add(this.label2);
            this.groupBox_in_count.Controls.Add(this.label_submit_lastTime);
            this.groupBox_in_count.Location = new System.Drawing.Point(81, 6);
            this.groupBox_in_count.Name = "groupBox_in_count";
            this.groupBox_in_count.Size = new System.Drawing.Size(189, 77);
            this.groupBox_in_count.TabIndex = 39;
            this.groupBox_in_count.TabStop = false;
            // 
            // groupBox_cloud_sync
            // 
            this.groupBox_cloud_sync.Controls.Add(this.label26);
            this.groupBox_cloud_sync.Controls.Add(this.label_sync_number);
            this.groupBox_cloud_sync.Controls.Add(this.label4);
            this.groupBox_cloud_sync.Controls.Add(this.label_sync_lastTime);
            this.groupBox_cloud_sync.Location = new System.Drawing.Point(279, 6);
            this.groupBox_cloud_sync.Name = "groupBox_cloud_sync";
            this.groupBox_cloud_sync.Size = new System.Drawing.Size(219, 77);
            this.groupBox_cloud_sync.TabIndex = 40;
            this.groupBox_cloud_sync.TabStop = false;
            // 
            // groupBox_dateTime_now
            // 
            this.groupBox_dateTime_now.Controls.Add(this.title_dateTime);
            this.groupBox_dateTime_now.Controls.Add(this.lable_dateTime_now);
            this.groupBox_dateTime_now.Location = new System.Drawing.Point(506, 6);
            this.groupBox_dateTime_now.Name = "groupBox_dateTime_now";
            this.groupBox_dateTime_now.Size = new System.Drawing.Size(195, 77);
            this.groupBox_dateTime_now.TabIndex = 41;
            this.groupBox_dateTime_now.TabStop = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // groupBox_Error
            // 
            this.groupBox_Error.Controls.Add(this.label1);
            this.groupBox_Error.Controls.Add(this.label_error_number);
            this.groupBox_Error.Location = new System.Drawing.Point(16, 6);
            this.groupBox_Error.Name = "groupBox_Error";
            this.groupBox_Error.Size = new System.Drawing.Size(56, 77);
            this.groupBox_Error.TabIndex = 43;
            this.groupBox_Error.TabStop = false;
            // 
            // label_state_network
            // 
            this.label_state_network.AutoSize = true;
            this.label_state_network.BackColor = System.Drawing.Color.Gray;
            this.label_state_network.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_state_network.ForeColor = System.Drawing.Color.DarkGray;
            this.label_state_network.Location = new System.Drawing.Point(528, 93);
            this.label_state_network.Name = "label_state_network";
            this.label_state_network.Size = new System.Drawing.Size(50, 15);
            this.label_state_network.TabIndex = 44;
            this.label_state_network.Text = "network";
            // 
            // label_state_print
            // 
            this.label_state_print.AutoSize = true;
            this.label_state_print.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label_state_print.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_state_print.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_state_print.Location = new System.Drawing.Point(77, 197);
            this.label_state_print.Name = "label_state_print";
            this.label_state_print.Size = new System.Drawing.Size(19, 13);
            this.label_state_print.TabIndex = 59;
            this.label_state_print.Text = "....";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label31.ForeColor = System.Drawing.Color.Gray;
            this.label31.Location = new System.Drawing.Point(17, 196);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(59, 13);
            this.label31.TabIndex = 60;
            this.label31.Text = "print state :";
            // 
            // label_range_weight_kg
            // 
            this.label_range_weight_kg.AutoSize = true;
            this.label_range_weight_kg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_range_weight_kg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label_range_weight_kg.Location = new System.Drawing.Point(128, 612);
            this.label_range_weight_kg.Name = "label_range_weight_kg";
            this.label_range_weight_kg.Size = new System.Drawing.Size(34, 15);
            this.label_range_weight_kg.TabIndex = 61;
            this.label_range_weight_kg.Text = ".........";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label41.Location = new System.Drawing.Point(27, 609);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(100, 20);
            this.label41.TabIndex = 62;
            this.label41.Text = "R weight kg :";
            // 
            // label_fix_validation
            // 
            this.label_fix_validation.AutoSize = true;
            this.label_fix_validation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_fix_validation.ForeColor = System.Drawing.Color.Gray;
            this.label_fix_validation.Location = new System.Drawing.Point(19, 249);
            this.label_fix_validation.Name = "label_fix_validation";
            this.label_fix_validation.Size = new System.Drawing.Size(105, 15);
            this.label_fix_validation.TabIndex = 78;
            this.label_fix_validation.Text = "detect validation ?";
            // 
            // label_display_mode
            // 
            this.label_display_mode.AutoSize = true;
            this.label_display_mode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_display_mode.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_display_mode.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_display_mode.Location = new System.Drawing.Point(16, 479);
            this.label_display_mode.Name = "label_display_mode";
            this.label_display_mode.Size = new System.Drawing.Size(72, 42);
            this.label_display_mode.TabIndex = 79;
            this.label_display_mode.Text = "NN";
            // 
            // label_display_detail
            // 
            this.label_display_detail.AutoSize = true;
            this.label_display_detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_display_detail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_display_detail.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_display_detail.Location = new System.Drawing.Point(19, 525);
            this.label_display_detail.Name = "label_display_detail";
            this.label_display_detail.Size = new System.Drawing.Size(49, 65);
            this.label_display_detail.TabIndex = 80;
            this.label_display_detail.Text = "(02)....\r\n(21)....\r\n(01)....\r\n(240)....\r\n(8008)....";
            // 
            // button_repair_data
            // 
            this.button_repair_data.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_repair_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button_repair_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button_repair_data.Location = new System.Drawing.Point(415, 114);
            this.button_repair_data.Name = "button_repair_data";
            this.button_repair_data.Size = new System.Drawing.Size(56, 26);
            this.button_repair_data.TabIndex = 191;
            this.button_repair_data.Text = "repair";
            this.button_repair_data.UseVisualStyleBackColor = false;
            this.button_repair_data.Visible = false;
            this.button_repair_data.Click += new System.EventHandler(this.button_repair_data_Click);
            // 
            // Form_macros_print
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(717, 694);
            this.Controls.Add(this.button_repair_data);
            this.Controls.Add(this.label_display_detail);
            this.Controls.Add(this.label_display_mode);
            this.Controls.Add(this.label_fix_validation);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label_range_weight_kg);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label_state_print);
            this.Controls.Add(this.label_state_network);
            this.Controls.Add(this.groupBox_Error);
            this.Controls.Add(this.groupBox_dateTime_now);
            this.Controls.Add(this.groupBox_cloud_sync);
            this.Controls.Add(this.groupBox_in_count);
            this.Controls.Add(this.label_state_cloud);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.groupBox_config_panel);
            this.Controls.Add(this.label_state_prop);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label_state_connDB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label_textQRCode);
            this.Controls.Add(this.checkBox_tools_show);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.pictureBoxQRCode);
            this.Controls.Add(this.label_image_output1);
            this.Controls.Add(this.button_synchronize);
            this.Controls.Add(this.label_textOutput);
            this.Controls.Add(this.button_execute_print);
            this.Controls.Add(this.button_gen_data);
            this.Controls.Add(this.button_clear_data);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label_title_output1);
            this.Controls.Add(this.label_title_input);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_macros_print";
            this.Opacity = 0.95D;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ThaiPET QRPrint";
            this.Load += new System.EventHandler(this.Form_macros_print_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQRCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_iconSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_macNo)).EndInit();
            this.groupBox_config_panel.ResumeLayout(false);
            this.groupBox_config_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_maximum_weight_g)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_minimum_weight_g)).EndInit();
            this.groupBox_setting_print.ResumeLayout(false);
            this.groupBox_setting_print.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_interval_time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventLog1)).EndInit();
            this.groupBox_in_count.ResumeLayout(false);
            this.groupBox_in_count.PerformLayout();
            this.groupBox_cloud_sync.ResumeLayout(false);
            this.groupBox_cloud_sync.PerformLayout();
            this.groupBox_dateTime_now.ResumeLayout(false);
            this.groupBox_dateTime_now.PerformLayout();
            this.groupBox_Error.ResumeLayout(false);
            this.groupBox_Error.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_title_input;
        private System.Windows.Forms.Label label_title_output1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button_clear_data;
        private System.Windows.Forms.Button button_gen_data;
        private System.Windows.Forms.Button button_execute_print;
        private System.Windows.Forms.Label label_textOutput;
        private System.Windows.Forms.Label lable_dateTime_now;
        private System.Windows.Forms.Button button_synchronize;
        private System.Windows.Forms.Label title_dateTime;
        private System.Windows.Forms.Label label_image_output1;
        private System.Windows.Forms.PictureBox pictureBoxQRCode;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_submit_number;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_sync_number;
        private System.Windows.Forms.CheckBox checkBox_tools_show;
        private System.Windows.Forms.Label label_textQRCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_error_number;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_state_connDB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_state_prop;
        private System.Windows.Forms.NumericUpDown numericUpDown_macNo;
        private System.Windows.Forms.NumericUpDown numericUpDown_iconSize;
        private System.Windows.Forms.ComboBox comboBox_levelQR;
        private System.Windows.Forms.GroupBox groupBox_config_panel;
        private System.Windows.Forms.TextBox textBox_iconPath;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_interval_time;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label_submit_lastTime;
        private System.Windows.Forms.TextBox textBox_text_readLine;
        private System.Windows.Forms.TextBox textBox_xml_path;
        private System.Windows.Forms.TextBox textBox_dbf_path;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label_state_cloud;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label_OADate_now;
        private System.Windows.Forms.Label label_sync_lastTime;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox_range_time;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label_cache_table_log;
        private System.Diagnostics.EventLog eventLog1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox_cloud_sync;
        private System.Windows.Forms.GroupBox groupBox_in_count;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox_dateTime_now;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox_sync_sheetChr;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.GroupBox groupBox_Error;
        private System.Windows.Forms.Label label_state_network;
        private System.Windows.Forms.Label label_state_print;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox_set_post_y;
        private System.Windows.Forms.TextBox textBox_set_post_x;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox_pictureBox_size;
        private System.Windows.Forms.TextBox textBox_set_post_size;
        private System.Windows.Forms.GroupBox groupBox_setting_print;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox_logger_path;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label_range_weight_kg;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.NumericUpDown numericUpDown_minimum_weight_g;
        private System.Windows.Forms.NumericUpDown numericUpDown_maximum_weight_g;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label_fix_validation;
        private System.Windows.Forms.TextBox textBox_txt_validation_query;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label_display_mode;
        private System.Windows.Forms.Label label_display_detail;
        private System.Windows.Forms.TextBox textBox_get_sheetChr;
        private System.Windows.Forms.Button button_repair_data;
    }
}