﻿#define XPRINT
using System;
using System.Data.OleDb; ////connect dbf
using System.Data.SqlClient; ////connect dbf
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using QRCoder;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Threading;
using System.Net.NetworkInformation;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Text.Json;
using System.Threading.Tasks;



namespace QRCoderDemo
{
   
    public partial class Form_macros_print : Form
    {

        public class DataSync
        {
            public string OASTAMP_ID { get; set; }
            public string PACKAGE_ID { get; set; }
            public string COMPANY_ID { get; set; }
            public string TYPE_STR { get; set; }
            public string WEIGHT_STR { get; set; }
            public string RMACNO_STR { get; set; }
            public string DSTAMP_ID { get; set; }
            public string QR_IMG_ID { get; set; }
        }



        XmlSerializer xs;
        List<QRCode_config> lq;
        public Form_macros_print()
        {
            InitializeComponent();

            lq = new List<QRCode_config>();
            xs = new XmlSerializer(typeof(List<QRCode_config>));
        }

        //public System.Timers.Timer t = new System.Timers.Timer();
        //Timer t = new Timer();
        System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
        private void Form_macros_print_Load(object sender, EventArgs e)
        {
            xmlReadAndCreate_exist();
            reset_data_instance();

            new_cache_table_routine();

            //timer interval
           // t.Interval = Program.QRconfig.interval_time; //1000;  //in milliseconds
           // t.Tick += new EventHandler(this.t_Tick); //System.Timers.Timer
            //start timer when form loads
           // t.Start();  //this will use t_Tick() method  

            DateTime DT_now = DateTime.Now;
            //routine_loop_chk(DT_now);
            Console.Write("x");
        }

        static string formate_dd_MM_yyyy = "";       
        private void t_Tick(object sender, EventArgs e)
        {
            /////present day now. on STEP > 1 > 2 > 3
            DateTime DT_now = DateTime.Now;
            double OADate_now = DT_now.ToOADate();
            string DT_now_str = DT_now.ToString("MM/dd/yyyy HH:mm:ss");
            string D_now_str = DT_now.ToString("MM/dd/yyyy");
            /////////////////////////////////////////////////////////////////
            formate_dd_MM_yyyy = DT_now.ToString("dd/MM/yyyy HH:mm:ss");
            lable_dateTime_now.Text = DT_now_str;

            AuthorizeGoogleApp_getDataPrint(); //////run loop time check
            //double dt_now = DateTime.Now.ToOADate();
            progressBar_tick_measure();
            
            //Program.QRconfig.rangeTimeChr
            if (Program.QRconfig.rangeTimeChr.ToString() != "")
            {
                routine_loop_chk(DT_now);
            }

            /////////////////////clear when on next day  open app
            if(Program.date_Str_last != D_now_str) {

                resetState_routine(":");
                checkState_routineLogDisplay();
                Program.date_Str_last = D_now_str; }


            //execute_DBFdata_existFile(true);////check exist table. (boolean)bool_syncTemp(spreadsheet)
        }

        private void routine_loop_chk(DateTime DT_now)
        {
           
            if (DT_now.ToString() == "") {
                DT_now = DateTime.Now; }
            double OADate_now = DT_now.ToOADate();
            string DT_now_str = DT_now.ToString("MM/dd/yyyy HH:mm:ss");
            string D_now_str = DT_now.ToString("MM/dd/yyyy");
            /////////////////////////////////////////////////////////////////
            formate_dd_MM_yyyy = DT_now.ToString("dd/MM/yyyy HH:mm:ss");
            lable_dateTime_now.Text = DT_now_str;

            //Program.QRconfig.rangeTimeChr   for()
            string[] range_Time_routine = Program.QRconfig.rangeTimeChr.Split(',');
            foreach (string range_Time_x in range_Time_routine)
            {
                try
                {
                    string[] Time_n_list = range_Time_x.Split('-');//range_Time_routine[range_Time_x];
                    string[] time_number_point_st = Time_n_list[0].Split(':');
                    string[] time_number_point_ed = Time_n_list[1].Split(':');
                    int point_n_st0 = 0,
                        point_n_st1 = 0,
                        point_n_st2 = 0,

                        point_n_ed0 = 0,
                        point_n_ed1 = 0,
                        point_n_ed2 = 0;

                    try { point_n_st0 = int.Parse(time_number_point_st[0]); } catch (Exception) { }
                    try { point_n_st1 = int.Parse(time_number_point_st[1]); } catch (Exception) { }
                    try { point_n_st2 = int.Parse(time_number_point_st[2]); } catch (Exception) { }

                    try { point_n_ed0 = int.Parse(time_number_point_ed[0]); } catch (Exception) { }
                    try { point_n_ed1 = int.Parse(time_number_point_ed[1]); } catch (Exception) { }
                    try { point_n_ed2 = int.Parse(time_number_point_ed[2]); } catch (Exception) { }


                    //convert time to OADate range of routine
                    DateTime DT_range_st = new DateTime(DT_now.Year, DT_now.Month, DT_now.Day, point_n_st0, point_n_st1, point_n_st2);
                    DateTime DT_range_ed = new DateTime(DT_now.Year, DT_now.Month, DT_now.Day, point_n_ed0, point_n_ed1, point_n_ed2);

                    double OA_range_st = DT_range_st.ToOADate();
                    double OA_range_ed = DT_range_ed.ToOADate();

                    if (OA_range_st <= OADate_now && OA_range_ed >= OADate_now)
                    {
                        ////////////////////////////////////////////////////////////////////////////execute on range time!!!
                        object[] data_row = { range_Time_x, true, OADate_now };
                        updateState_routine(data_row);
                        checkState_routineLogDisplay();

                        label_OADate_now.Text = DT_now_str + " (it's time)";
                        label_OADate_now.BackColor = System.Drawing.Color.LimeGreen;
                        label_OADate_now.ForeColor = System.Drawing.Color.Black;
                    }
                    else
                    {
                        label_OADate_now.Text = DT_now_str + " (Not arrive yet)";
                        label_OADate_now.BackColor = System.Drawing.Color.DimGray;
                        label_OADate_now.ForeColor = System.Drawing.Color.White;
                    }

                    /////////////////////////////////////routine network time protocol : initialize synchronize(1).
                    if (!Program.sync_network_time_protocol_bool)
                    {

                        check_state_network_available();/////is network available
                        if (Program.netWork_available_bool)
                        {
                            Program.sync_network_time_protocol_bool = true;
                            string string_internet_time_compare = GetServerTime().ToString("MM/dd/yyyy HH:mm");
                            string string_machine_time_compare = DT_now.ToString("MM/dd/yyyy HH:mm");

                            if (string_internet_time_compare != string_machine_time_compare)
                            {
                                Program.sync_network_time_protocol_bool = run_batch_sync_time();
                                label_state_network.Text = "sync time.";
                            }

                            if (string_internet_time_compare == string_machine_time_compare)
                            {
                                label_state_network.Text = "current time.";
                            }
                        }
                    }
                }
                catch (Exception ex) { label_state_cloud.Text = ex.Message.ToString(); event_logger_detect(ex.Message.ToString()); }
            }
            Console.Write("x");
        }

        private bool run_batch_sync_time()
        { 
            try

            {
                ProcessStartInfo procInfo = new ProcessStartInfo();

                procInfo.UseShellExecute = true;

                procInfo.FileName = @"sync_time.bat";  //The file in that DIR.

                procInfo.WorkingDirectory = @""; //The working DIR.

                procInfo.Verb = "runas";// as administrator

                Process.Start(procInfo);  //Start that process.  
                
                return true;
            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                return false;
            }
        }

        private void ReSync()
        {
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = "NET";
            p.StartInfo.Arguments = "TIME \\\\SERVERNAME /SET /YES";
            p.Start();
            p.WaitForExit();
        }
        

        private void check_state_network_available()
        {

            get_netWork_Available();
            bool Net_connection = Program.netWork_available_bool;

            if (Net_connection == true)
            {
                label_state_network.BackColor = System.Drawing.Color.LimeGreen;
                label_state_network.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                label_state_network.BackColor = System.Drawing.Color.Red;
                label_state_network.ForeColor = System.Drawing.Color.White;
            }
            
        }
        private void get_netWork_Available()
        {
            bool Net_connection = NetworkInterface.GetIsNetworkAvailable();
            Program.netWork_available_bool = Net_connection;
        }


        #region create cache dataTable.
        private void new_cache_table_routine()
        {
           
            DataTable dt_cache_rt = Program.cache_table_routine;
            dt_cache_rt.Clear();
            //use data table.
            dt_cache_rt.Columns.Add(new DataColumn("routine_rangeTimeChr", typeof(string)));
            dt_cache_rt.Columns.Add(new DataColumn("routine_executeBool", typeof(bool)));
            dt_cache_rt.Columns.Add(new DataColumn("routine_OADate", typeof(double)));    
        }

        private void updateState_routine(object[] data_row)
        {
            ///////////allow be start
            if (Program.working_Routine_bool) { return; }
            Program.working_Routine_bool = true;

            //--------------------------------------------------------------------------------------------------------------------------------------------------

            ////check null data_row.length
            if (data_row.Length <= 0) { return; }
            ///expand data from data_row {0,1,2,3} from upsert
            object new_rangeTimeChr = data_row[0],
                   new_executeBool = data_row[1],
                   new_OADateNow = data_row[2];
           
            DataTable dt_cache_rt = Program.cache_table_routine;
            /////search and update
            if (dt_cache_rt.Rows.Count > 0)
            {
                DataRow[] result = dt_cache_rt.Select("routine_rangeTimeChr = '" + new_rangeTimeChr + "'");  //tring sortOrder = "CompanyName ASC";
                foreach (DataRow row in result)
                {
                    var ItemArray_column = row.ItemArray;
                    string source_rangeTimeChr = ItemArray_column[0].ToString();
                    string source_executeBool = ItemArray_column[1].ToString();
                    string source_OADateNow = ItemArray_column[2].ToString();
                    /////update static state.
                    ////////////////////////////////state_routine_executeBool = new_executeBool;

                    /////compare state(text).
                    if (source_executeBool.ToString() != new_executeBool.ToString()) //source_executeBool
                    {
                        execute_DBFdata_existFile(true);////check exist table. (boolean)bool_syncTemp(spreadsheet)
                        row[0] = new_rangeTimeChr;//"{0}, {1}, {2}"
                        row[1] = new_executeBool;//"{0}, {1}, {2}"
                        row[2] = new_OADateNow;//"{0}, {1}, {2}" 
                        //Console.WriteLine("{0}, {1}, {2}", row[0], row[1], row[2]);
                    }
                }
                /////not found exist value.
                if (result.Length <= 0)
                {
                    execute_DBFdata_existFile(true);////check exist table. (boolean)bool_syncTemp(spreadsheet)
                    dt_cache_rt.Rows.Add(data_row);
                }
            }
            else
            {
                execute_DBFdata_existFile(true);////check exist table. (boolean)bool_syncTemp(spreadsheet)
                dt_cache_rt.Rows.Add(data_row);
            }

            //--------------------------------------------------------------------------------------------------------------------------------------------------
            ///////////close for end
            Program.working_Routine_bool = false;
        }

        private void checkState_routineLogDisplay()
        {
            DataTable dt_cache_rt = Program.cache_table_routine;

            string _label_cache_table_log = "";
            /////search and update
            if (dt_cache_rt.Rows.Count > 0)
            {
                ////////////////display label dataTable.text
                DataRow[] result_all = dt_cache_rt.Select("routine_OADate > 0");  //tring sortOrder = "CompanyName ASC";
                foreach (DataRow row in result_all)
                {
                    //var s = row.ItemArray.Length;
                    var ItemArray_column = row.ItemArray;
                    string source_rangeTimeChr = ItemArray_column[0].ToString();
                    string source_executeBool = ItemArray_column[1].ToString();
                    string source_OADateNow = ItemArray_column[2].ToString();

                    _label_cache_table_log += source_rangeTimeChr + " ◙ " + source_executeBool + " ◙ " + DateTime.FromOADate(double.Parse(source_OADateNow)).ToString("MM/dd/yy HH:mm:ss") + "\n";                   
                }
            }
            /////update static state log.
            label_cache_table_log.Text = _label_cache_table_log;
        }

        private void resetState_routine(string new_rangeTimeChr)
        {
            ///////////allow be start
            if (Program.working_Routine_bool) { return; }
            Program.working_Routine_bool = true;

            //--------------------------------------------------------------------------------------------------------------------------------------------------

            DataTable dt_cache_rt = Program.cache_table_routine;
            /////like search and reset updated
            if (dt_cache_rt.Rows.Count > 0)
            {
                DataRow[] result = dt_cache_rt.Select("routine_rangeTimeChr like '%" + new_rangeTimeChr + "%'");  //tring sortOrder = "CompanyName ASC";
                foreach (DataRow row in result)
                {
                    var ItemArray_column = row.ItemArray;
                    string source_rangeTimeChr = ItemArray_column[0].ToString();
                    string source_executeBool = ItemArray_column[1].ToString();
                    string source_OADateNow = ItemArray_column[2].ToString();
                    /////update static state.
                    ////////////////////////////////state_routine_executeBool = new_executeBool;

                    /////compare state(text).
                    if (source_executeBool.ToString() == "True") //source_executeBool
                    {
                        //row[0] = source_rangeTimeChr;//"{0}, {1}, {2}"
                        row[1] = false;//"{0}, {1}, {2}"
                        row[2] = DateTime.Now.ToOADate();//"{0}, {1}, {2}" 
                        //Console.WriteLine("{0}, {1}, {2}", row[0], row[1], row[2]);
                    }
                }
            }

            //--------------------------------------------------------------------------------------------------------------------------------------------------
            ///////////close for end
            Program.working_Routine_bool = false;
        }
        #endregion create cache dataTable.

        private void progressBar_tick_measure()
        {
            #region Negative dt_now from dt_last

            //string[] lb_TextOut = label_textOutput.Text.Split('\n');
            //double dt_last = dt_now;
            //if (lb_TextOut[0] != "" && lb_TextOut[0].IndexOf("...") < 0) { dt_last = double.Parse(lb_TextOut[0]); }

            #endregion Negative dt_now from dt_last

            int percent_value = 0;
            if (textBox1.Text != "") { percent_value = 30; }
            if (textBox2.Text != "") { percent_value += 30; }

            if (textBox3.Text != "") { percent_value += 35; }
            else if ((textBox4.Text != "" || textBox5.Text != "") && textBox1.Text != "") { percent_value = 95; }

            if (textBox1.Focused && textBox1.Text == "") { percent_value = 6; } //(lb_TextOut[0] == "" || lb_TextOut[0].IndexOf("...") > -1)) { percent_value = 3; }
            //else if ((dt_now - dt_last) > 0.004) { percent_value += 5; label5.Text = (dt_now - dt_last).ToString() + " <> " + dt_now.ToString() + " - " + dt_last.ToString(); }
            //else if (textBox1.Focused && lb_TextOut[0] == "") { percent_value = 3; }
            //else { percent_value = 3; }
            progressBar1.Value = percent_value;
        }

        #region dateTimeServer
        public static DateTime GetServerTime()
        {
            var myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
            var response = myHttpWebRequest.GetResponse();
            string todaysDates = response.Headers["date"];
            return DateTime.ParseExact(todaysDates,
                                       "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                                       CultureInfo.InvariantCulture.DateTimeFormat,
                                       DateTimeStyles.AssumeUniversal);
        }

        public static DateTime GetServerTime2()
        {
            DateTime dateTime = DateTime.MinValue;
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://www.microsoft.com");
            request.Method = "GET";
            request.Accept = "text/html, application/xhtml+xml, */*";
            request.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
            request.ContentType = "application/x-www-form-urlencoded";
            request.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                string todaysDates = response.Headers["date"];

                dateTime = DateTime.ParseExact(todaysDates, "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                    System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat, System.Globalization.DateTimeStyles.AssumeUniversal);
            }

            return dateTime;
        }
        #endregion

        private void checkBox_show_CheckStateChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.IndexOf("kg") < 0 &&
                textBox2.Text.IndexOf("kg") < 0 &&
                textBox3.Text.IndexOf("kg") < 0 &&
                textBox4.Text.IndexOf("kg") < 0 &&
                textBox5.Text.IndexOf("kg") < 0) { textBox1.Focus(); }
            else { textBox5.Focus(); }

            string chk_box1 = checkBox_tools_show.CheckState.ToString();

            if(chk_box1.ToLower() == "checked")
            {
                button_repair_data.Visible = true;
                button_clear_data.Visible = true;
                button_gen_data.Visible = true;
                button_execute_print.Visible = true;
                button_synchronize.Visible = true;
                groupBox_config_panel.Visible = true;
            }
            else
            {
                button_repair_data.Visible = false;
                button_clear_data.Visible = false;
                button_gen_data.Visible = false;
                button_execute_print.Visible = false;
                button_synchronize.Visible = false;
                groupBox_config_panel.Visible = false;

                xmlWriteAndChangedxx_exist();

                label_range_weight_kg.Text = (numericUpDown_maximum_weight_g.Value / 1000).ToString("##0.000") + " - " + (numericUpDown_minimum_weight_g.Value / 1000).ToString("##0.000");
            }
        }
        
        #region textBox keyDown
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            //"Enter" >>> result from e.keyValue is "13" , e.KeyCode is "return"
            execute_keyEvent_stepOff(e);
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            execute_keyEvent_stepOff(e);
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            execute_keyEvent_stepOff(e);
        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            execute_keyEvent_stepOff(e);
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            //MessageBox.Show(e.KeyData.ToString());
            execute_keyEvent_stepOff(e);
        }

        private void checkBox_show_KeyDown(object sender, KeyEventArgs e)
        {
            execute_keyEvent_stepOff(e);
        }
        #endregion textBox keyDown
        
        /*
          dynamic stuff1 = Newtonsoft.Json.JsonConvert.DeserializeObject(a_str);
          string Text = stuff1[0].hws[2];
          Console.WriteLine(Text);*/
        //List<IList<string>> A_s;
        private bool bool_validation_check(string str_txt_query)
        {
            string msg_error = "detect validation";//กำหนดสติง 
            bool validation_bool = true;
            if (str_txt_query.Trim() == "") { 
                return validation_bool; 
            }

            string[] ARRAY_JSON = str_txt_query.Split(','); //kg:12,g/pcs:15
            ////merge fild of type
            for (int i = 0; i < ARRAY_JSON.Length; i++)
            {
                string[] field_range = ARRAY_JSON[i].Split(':');
                if (field_range[1] == "" || field_range[0] == "") { continue; }

                string key_txt_find = field_range[0];
                int char_valit_number = int.Parse(field_range[1]);
                bool SSS_bool = validation_newLine_result(key_txt_find, char_valit_number);

                /*if (validation_newLine_result(key_txt_find, char_valit_number))
                {
                    validation_bool = false;
                    msg_error += "\n '" + field_range[0] + "':" + field_range[1];
                    label_fix_validation.ForeColor = System.Drawing.Color.Red;
                    label_fix_validation.Text = msg_error;
                    break;
                }*/ //else
                {
                    msg_error += "\n pass..";
                    label_fix_validation.ForeColor = System.Drawing.Color.LimeGreen;
                    label_fix_validation.Text = msg_error;
                }
            }
            return validation_bool;
        }

        private bool validation_newLine_result(string key_txt_find, int char_valit_number)
        {
            string _newLine_result_txt = (Program.newLine_result_txt);
            string[] _newLine_result_txt_split = _newLine_result_txt.Split(',');

            bool validation_bool = true;
            if (key_txt_find == "" || char_valit_number <= 0 || _newLine_result_txt == "") { validation_bool = false; }

            for (int i = 0; i < _newLine_result_txt_split.Length; i++)
            {
                if (_newLine_result_txt_split[i].IndexOf(key_txt_find) > -1)
                {
                    var text_length = _newLine_result_txt_split[i].Length;
                    if (_newLine_result_txt_split[i].Length == char_valit_number) { validation_bool = true; }
                    else { validation_bool = false; }
                    break;
                }
            }
            return validation_bool;
        }
        ///set name of device.
        private void execute_keyEvent_stepOff(KeyEventArgs e)
        {
            if (e.KeyValue.ToString() == "13" || e.KeyCode.ToString() == "return")
            {
                //submit data on label and get dateTime now.
                display_on_label();
                event_logger_detect(null); //build value on "Program.newLine_result_txt"
                render_data_print_save("");
            }
        }

        private void render_data_print_save(string result_textOutput)
        {
         
            string str_txt_query = Program.QRconfig.txt_validation_query;
            bool validation_bool = bool_validation_check(str_txt_query);

            if (result_textOutput != "") { label_textOutput.Text = result_textOutput; validation_bool = true; } ////by pass.

            #region parse data prepare date to QRCode && check is correct weight data    
            string label_data = label_textOutput.Text;
            // Split string on spaces (this will separate all the words).
            string[] label_split_array = label_data.Split('\n');
            //var x = new GS1_data { Package_Id = "", Company_Id = "", Type_Str = "", Weight_Str = "", rMacNo_Str = "", DateTime_Id = "" };
            Program.GS1_data = new GS1_data();
            //var dbf_Data = new DBF_data { OAStamp_Id = 0, Package_Id = "", Company_Id = "", Type_Str = "", Weight_Str = "", rMacNo_Str = "", dStamp_Id = "", QR_IMG_ID = "" };
            Program.DBF_data = new DBF_data();
            //(21)PW2019042611293702(01)8859229399998(91)PACKAGE(3303)004140(240)WEIGHTSCALE02(8008)190426112937

            var _weight_mac_chk = "";
            var _logistic_weight = "(3300)"; //default.

            var char_left_digit = 3;
            var char_right_digit = 3;
            int charLeft_length_temp = 0;
            int charRight_length_temp = 0;

            ////convert OaDate to dd/MM/yyyy hh:mm:ss type double only.               
            DateTime Date_now = DateTime.FromOADate(Double.Parse(label_split_array[0]));
            var rMac_number = "0" + Program.QRconfig.macNo;

            string _Weight_Str = "", _PackageId_str = "", _ref_data_str = "", _serial_str = "", _PackageSecondId_str = "";
            for (int i = 0; i < label_split_array.Length; i++)
            {
                if (label_split_array[i].Trim() == "") { continue; }

                //convert by without OaDate.
                if (label_split_array[i].IndexOf("kg") > -1 || (label_split_array[i].IndexOf(".") > -1 && label_split_array[i].IndexOf("pcs") <= -1 && label_split_array[i].IndexOf("/") <= -1)) //label_split_array[i].IndexOf('.') > -1
                {
                    label_split_array[i] = label_split_array[i].Replace("kg", "");
                    label_split_array[i] = label_split_array[i].Trim();
                    _weight_mac_chk = label_split_array[i];


                    string charLeft = "";
                    string charRight = "";
                    if (label_split_array[i].IndexOf('.') > -1)
                    {
                        string[] charN_array = label_split_array[i].Split('.');
                        ////////////combination text of weight step for extend "0";
                        charLeft = charN_array[0];
                        charRight = charN_array[1];
                    }
                    else
                    {
                        ////////////combination text of weight step for extend "0";
                        charLeft = label_split_array[i];
                    }

                    if (charRight.Length > 4) { continue; } ////ป้องกันเวลาเข้ามา

                    charLeft_length_temp = charLeft.Length;
                    if (charRight.Length > 0 && charLeft_length_temp - 3 > 0) { char_right_digit = charLeft_length_temp - 3; }
                    for (var cL = char_left_digit; charLeft.Length < cL;) { charLeft = "0" + charLeft; }
                    charRight_length_temp = charRight.Length;
                    if (charRight.Length > 0)
                    {
                        for (var cR = char_right_digit; charRight.Length < cR;) { charRight = charRight + "0"; }
                    }

                    _Weight_Str += charLeft + charRight;
                    int cTotal = char_left_digit + char_right_digit;
                    for (var cT = cTotal; _Weight_Str.Length < cT;) { _Weight_Str = "0" + _Weight_Str;  }



                    _logistic_weight = "(330" + charRight_length_temp + ")";
                }
                /////detect PW {xxxxxxxxxxxxxxxx}
                if (label_split_array[i].Length >= 14 &&//&& i > 0
                   label_split_array[i].ToLower().Trim().IndexOf("pw") > -1)
                {
                    _PackageId_str = label_split_array[i].ToUpper();
                }

                if(label_split_array[i].IndexOf("m_id") > -1)
                {
                    _serial_str = label_split_array[i];
                    _serial_str = _serial_str.Replace("m_id", "");
                    _serial_str = _serial_str.Trim();
                }
                if(label_split_array[i].IndexOf("barcode_id") > -1)
                {
                    _ref_data_str = label_split_array[i];
                    _ref_data_str = _ref_data_str.Replace("barcode_id", "");
                    _ref_data_str = _ref_data_str.Trim();
                }
            }

            try
            {

                #region expand value
                #region expand value packageweight standard
                /*
                string date_string_number_full = Date_now.ToString("yyyyMMddHHmmss").ToString();
                string date_string_number_21 = date_string_number_full;
                int number_sub_length = 2;
                int number_full_Length = date_string_number_full.Length - number_sub_length;
                string date_string_number_8008 = date_string_number_full.Substring(number_sub_length, number_full_Length); */
                double OADate_float = double.Parse(label_split_array[0]);
                string PackageId_str = "PW" + Date_now.ToString("yyyyMMddHHmmss") + rMac_number;
                string CompanyId_str = "8859229399998";
                string Type_Str = "PACKAGE";
                string Weight_Str = _Weight_Str;
                string rMacNo_Str = "WEIGHTSCALE" + rMac_number;
                string DateTimeId_Str = Date_now.ToString("yyMMddHHmmss");
                string TimeFormate_Str = Date_now.ToString("HH:mm:ss");
                string DTimeFormate_Str = Date_now.ToString("MM/dd/yy HH:mm:ss");
                string PackageSecondId_str = ""; //"PW" + Date_now.ToString("yyyyMMddHHmmss") + rMac_number;
                string serial_str = _serial_str;
                string ref_data_str = _ref_data_str;
                #endregion expand value packageweight standard

                // r_e = Int32.Parse(_weight_mac_chk);
                #region expand sub weight 20-30kg #weight_no_01-02
                if (Double.Parse(_weight_mac_chk) >= 22 && Double.Parse(_weight_mac_chk) <= 30 && (int.Parse(rMac_number) >= 1 && int.Parse(rMac_number) <= 2))
                {
                    OADate_float = double.Parse(label_split_array[0]);
                    PackageId_str = "PM" + Date_now.ToString("yyyyMMddHHmmss") + rMac_number;
                    CompanyId_str = "8859229399998";
                    Type_Str = "MATERIAL";
                    Weight_Str = _Weight_Str;
                    rMacNo_Str = "WEIGHTSCALE" + rMac_number;
                    DateTimeId_Str = Date_now.ToString("yyMMddHHmmss");
                    TimeFormate_Str = Date_now.ToString("HH:mm:ss");
                    DTimeFormate_Str = Date_now.ToString("MM/dd/yy HH:mm:ss");
                }
                #endregion expand sub weight 20-30kg #weight_no_01-02
                /*
                #region expand sub weight 20-30kg #weight_no_03 
                if (Double.Parse(_weight_mac_chk) >= 22 && (Double.Parse(_weight_mac_chk) <= 30 && int.Parse(rMac_number) >= 3))
                {
                    OADate_float = double.Parse(label_split_array[0]);
                    PackageId_str = "RM" + Date_now.ToString("yyyyMMddHHmmss") + rMac_number;
                    CompanyId_str = "8859229399998";
                    Type_Str = "MATERIAL";
                    Weight_Str = _Weight_Str;
                    rMacNo_Str = "WEIGHTSCALE" + rMac_number;
                    DateTimeId_Str = Date_now.ToString("yyMMddHHmmss");
                    TimeFormate_Str = Date_now.ToString("HH:mm:ss");
                    DTimeFormate_Str = Date_now.ToString("MM/dd/yy HH:mm:ss");
                }
                #endregion expand sub weight 20-30kg #weight_no_03 

                #region expand sub weight 900-1500kg #weight_no_03  
                if (Double.Parse(_weight_mac_chk) >= 900 && (Double.Parse(_weight_mac_chk) <= 1500 && int.Parse(rMac_number) >= 3))
                {
                    OADate_float = double.Parse(label_split_array[0]);
                    PackageId_str = "RM" + serial_str + rMac_number;
                    PackageSecondId_str = "RM" + Date_now.ToString("yyyyMMddHHmmss") + rMac_number;
                    CompanyId_str = "8859229399998";
                    Type_Str = "MATERIAL";
                    Weight_Str = _Weight_Str;
                    rMacNo_Str = "WEIGHTSCALE" + rMac_number;
                    DateTimeId_Str = Date_now.ToString("yyMMddHHmmss");
                    TimeFormate_Str = Date_now.ToString("HH:mm:ss");
                    DTimeFormate_Str = Date_now.ToString("MM/dd/yy HH:mm:ss");
                }
                #endregion expand sub weight 900-1500kg #weight_no_03 
                */
                
                #region expand expand sub weight 10-17kg
                /*
                if (Double.Parse(_weight_mac_chk) >= 10 && Double.Parse(_weight_mac_chk) <= 20) //&& int.Parse(rMac_number) >= 3
                {
                    OADate_float = double.Parse(label_split_array[0]);
                    PackageId_str = "LM" + Date_now.ToString("yyyyMMddHHmmss") + rMac_number;
                    CompanyId_str = "8859229399998";
                    Type_Str = "MATERIAL";
                    Weight_Str = _Weight_Str;
                    rMacNo_Str = "WEIGHTSCALE" + rMac_number;
                    DateTimeId_Str = Date_now.ToString("yyMMddHHmmss");
                    TimeFormate_Str = Date_now.ToString("HH:mm:ss");
                    DTimeFormate_Str = Date_now.ToString("MM/dd/yy HH:mm:ss");
                }
                */
                #endregion expand expand sub weight 10-17kg  

                #endregion expand value

                #region check replace PackageId_str use cal "repeat print"
                if (_PackageId_str != "") { PackageId_str = _PackageId_str; }
                #endregion  check replace PackageId_str use cal "repeat print"

                #region combine to label_textDisplay >>> QRCode_data_img
                Program.GS1_data.Package_Id = "(21)" + PackageId_str;
                Program.GS1_data.Company_Id = "(01)" + CompanyId_str;
                Program.GS1_data.Type_Str = "(91)" + Type_Str;
                Program.GS1_data.Weight_Str = _logistic_weight + _Weight_Str; //"(3303)" + _Weight_Str;
                Program.GS1_data.rMacNo_Str = "(240)" + rMacNo_Str;
                Program.GS1_data.DateTime_Id = "(8008)" + DateTimeId_Str;

                string secondary_serial_number = "(250)" + PackageSecondId_str;
                string ref_new_type = "(90)" + ref_data_str;
                if (ref_data_str == "") { ref_new_type = ""; secondary_serial_number = ""; }

                string QR_IMG_ID = Program.GS1_data.Package_Id +
                                   secondary_serial_number +
                                   Program.GS1_data.Company_Id +
                                   Program.GS1_data.Type_Str +
                                   Program.GS1_data.Weight_Str +
                                   Program.GS1_data.rMacNo_Str +
                                   ref_new_type +
                                   Program.GS1_data.DateTime_Id;

                if (PackageSecondId_str != "") {
                                   QR_IMG_ID = secondary_serial_number +
                                   Program.GS1_data.Package_Id +
                                   //Program.GS1_data.Company_Id +
                                   //Program.GS1_data.Type_Str +
                                   Program.GS1_data.Weight_Str +
                                   //Program.GS1_data.rMacNo_Str +
                                   ref_new_type +
                                   Program.GS1_data.DateTime_Id;
                }
                #endregion combine to label_textDisplay >>> QRCode_data_img

                var data = new DataSync()
                {
                    OASTAMP_ID = OADate_float.ToString(),
                    PACKAGE_ID = PackageId_str,
                    COMPANY_ID = CompanyId_str,
                    TYPE_STR = Type_Str,
                    WEIGHT_STR = _logistic_weight + _Weight_Str,
                    RMACNO_STR = rMacNo_Str,
                    DSTAMP_ID = DateTimeId_Str,
                    QR_IMG_ID = QR_IMG_ID
                };

                string jsonString = System.Text.Json.JsonSerializer.Serialize(data);
                var redata = System.Text.Json.JsonSerializer.Deserialize<DataSync>(jsonString);
                FileInfo info = new FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location);
               string path = info.DirectoryName + @"\SyncQueue\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".json";
                File.WriteAllText(path, jsonString);
                var fields = redata.GetType().GetProperties();


                #region DB data parse
                Program.DBF_data.OAStamp_Id = OADate_float;
                Program.DBF_data.Package_Id = PackageId_str;
                Program.DBF_data.Company_Id = CompanyId_str;
                Program.DBF_data.Type_Str = Type_Str;
                Program.DBF_data.Weight_Str = Weight_Str;
                Program.DBF_data.rMacNo_Str = rMacNo_Str;
                Program.DBF_data.dStamp_Id = DateTimeId_Str;
                Program.DBF_data.QR_IMG_ID = QR_IMG_ID;
                #endregion  DB data parse

                //update textBox,label
                label_textQRCode.Text = QR_IMG_ID;
                result_textOutput = QR_IMG_ID;

                //char_left_digit char_right_digit chk_Weight_Str
                string chk_Weight_Str = Weight_Str;
                int charTotal_set = chk_Weight_Str.Length + (char_right_digit) - (charRight_length_temp);
                // charLeft_length_temp charRight_length_temp
                for (var charTotalChk = charTotal_set; chk_Weight_Str.Length < charTotalChk;)  { chk_Weight_Str = chk_Weight_Str + "0"; }   


                ///min(96) && max(98)
                if (label_textQRCode.Text.Length < 96 || int.Parse(chk_Weight_Str) <= Program.QRconfig.minimum_weight_g || int.Parse(chk_Weight_Str) >= Program.QRconfig.maximum_weight_g || !validation_bool)
                { setCounter(1, label_error_number, null); clear_data_afterComplete(); return; } //incomplete of data

                setCounter((1), label_submit_number, label_submit_lastTime);

                insertDBF_row();
                #endregion parse data prepare date to QRCode && check is correct weight data

                //example value [2017-09-07,12:01:21, 3.595 kg, 20.800 g/pcs, 173 pcs]
                //convert from present data and exicute to GS1(TPI)
                //76x76&data=(21)PW2019042611293702(01)8859229399998(91)PACKAGE(3303)004140(240)WEIGHTSCALE02(8008)190426112937
                //gen picture from GS1(TPI) CODE

                RenderQrCode();

                //button_synchronize insert data.DBF
                //prepare tempate for printer 4*4cm
                //exicute print
#if PRINT
                string chk_box1 = checkBox_tools_show.CheckState.ToString();
                if (chk_box1.ToLower() != "checked" && result_textOutput != "")
                { printDocument1.Print(); }

                //pictureBoxQRCode.Image  raw_Weight_Str PackageId_str

                //(++ counter number)

                //Luteen for loop sync to cloud
#endif
                clear_data_afterComplete();
            }
            catch (Exception ex)
            {
                label_state_print.Text = ex.Message.ToString();
                label_state_print.ForeColor = System.Drawing.Color.White;
                label_state_print.BackColor = System.Drawing.Color.Red;

                event_logger_detect(ex.Message.ToString());

                setCounter(1, label_error_number, null); clear_data_afterComplete(); return;
            }
        }
        /*
        private void direct_img_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
#region combine to label_textDisplay >>> QRCode_data_img.
            string QR_IMG_ID = "";
            //update textBox,label
            label_textQRCode.Text = QR_IMG_ID;
#endregion combine to label_textDisplay >>> QRCode_data_img.

#region get sheet target to list print

#endregion get sheet target to list print
            /////just do on step 1-2-3 
            RenderQrCode();
            //button_synchronize insert data.DBF
            //prepare tempate for printer 4*4cm
            //exicute print
            string chk_box1 = checkBox_tools_show.CheckState.ToString();
            if (chk_box1.ToLower() != "checked")
            { printDocument1.Print(); }        
        }printDocument1_PrintPage
        */
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                string[] pictureBox_size_list = Program.QRconfig.pictureSizeSlt.Split('*');
                float float_post_x = float.Parse(Program.QRconfig.txt_xPoint);
                float float_post_y = float.Parse(Program.QRconfig.txt_yPoint);
                float text_post_size = float.Parse(Program.QRconfig.txt_sizePost);

                pictureBoxQRCode.Width = int.Parse(pictureBox_size_list[0]);
                pictureBoxQRCode.Height = int.Parse(pictureBox_size_list[1]);

                if (Program.DBF_data.Weight_Str.ToString() == "")
                { return; }
                Font Font1 = new Font("Arial", text_post_size, FontStyle.Regular);  //(8  for 166*166),(8  for 156*156),(7  for 146*146 <4*4>)
                string A_str = "";
                //if (Program.DBF_data.Weight_Str.ToString().Length == 8) { A_str = Convert.ToDouble(Program.DBF_data.Weight_Str.Insert(4, ".")).ToString("####.0000");  }
                var _logistic_weight = Program.GS1_data.Weight_Str;
                var _idx_for_insert_array = _logistic_weight.ToString().Split(')');
                string _idx_for_insert = _idx_for_insert_array[0].ToString();
                _idx_for_insert = _idx_for_insert.Substring(4, 1);
                int result_insert = Int32.Parse(_idx_for_insert);
                int result_all_length = Program.DBF_data.Weight_Str.Length;

                ///////////extend zero.
                string zero_extend = "";
                string digit_formate = "###.";
                for (var zero_c = result_insert; zero_extend.Length < zero_c;) {
                    zero_extend = zero_extend + "0"; }
                if (result_insert > 0) { A_str = Convert.ToDouble(Program.DBF_data.Weight_Str.Insert(result_all_length - result_insert, ".")).ToString(digit_formate + zero_extend); }
                else { A_str = Convert.ToDouble(Program.DBF_data.Weight_Str).ToString(digit_formate + zero_extend); }               
                string data = Program.DBF_data.Package_Id + " " + A_str;
                            
                Bitmap myBitmap1 = new Bitmap(pictureBoxQRCode.Width, pictureBoxQRCode.Height);
                //myBitmap1 = sharpen(myBitmap1);
                pictureBoxQRCode.DrawToBitmap(myBitmap1, new Rectangle(0, 0, pictureBoxQRCode.Width, pictureBoxQRCode.Height));
                e.Graphics.DrawImage(myBitmap1, 0, 0);
                e.Graphics.DrawString(data, Font1, Brushes.Black, float_post_x, float_post_y);//(9 162 for 166*166),(9 152 for 156*156),(9 142  for 146*146 <4*4>)
                                                                          //e.Graphics.DrawString(data, Font1, Brushes.Black, 20, 20);

                label_state_print.Text = "normal printed";
                label_state_print.ForeColor = System.Drawing.Color.Black;
                label_state_print.BackColor = System.Drawing.Color.LimeGreen;
            }
            catch (Exception ex)
            {
                label_state_print.Text = ex.Message.ToString();
                label_state_print.ForeColor = System.Drawing.Color.White;
                label_state_print.BackColor = System.Drawing.Color.Red;

                event_logger_detect(ex.Message.ToString());
            }
        }

        public static Bitmap sharpen(Bitmap image)
        {
            Bitmap sharpenImage = new Bitmap(image.Width, image.Height);

            int filterWidth = 3;
            int filterHeight = 3;
            int w = image.Width;
            int h = image.Height;

            double[,] filter = new double[filterWidth, filterHeight];

            filter[0, 0] = filter[0, 1] = filter[0, 2] = filter[1, 0] = filter[1, 2] = filter[2, 0] = filter[2, 1] = filter[2, 2] = -1;
            filter[1, 1] = 9;

            double factor = 1.0;
            double bias = 0.0;

            System.Drawing.Color[,] result = new System.Drawing.Color[image.Width, image.Height];

            for (int x = 0; x < w; ++x)
            {
                for (int y = 0; y < h; ++y)
                {
                    double red = 0.0, green = 0.0, blue = 0.0;
                    System.Drawing.Color imageColor = image.GetPixel(x, y);

                    for (int filterX = 0; filterX < filterWidth; filterX++)
                    {
                        for (int filterY = 0; filterY < filterHeight; filterY++)
                        {
                            int imageX = (x - filterWidth / 2 + filterX + w) % w;
                            int imageY = (y - filterHeight / 2 + filterY + h) % h;
                            red += imageColor.R * filter[filterX, filterY];
                            green += imageColor.G * filter[filterX, filterY];
                            blue += imageColor.B * filter[filterX, filterY];
                        }
                        int r = Math.Min(Math.Max((int)(factor * red + bias), 0), 255);
                        int g = Math.Min(Math.Max((int)(factor * green + bias), 0), 255);
                        int b = Math.Min(Math.Max((int)(factor * blue + bias), 0), 255);

                        result[x, y] = System.Drawing.Color.FromArgb(r, g, b);
                    }
                }
            }
            for (int i = 0; i < w; ++i)
            {
                for (int j = 0; j < h; ++j)
                {
                    sharpenImage.SetPixel(i, j, result[i, j]);
                }
            }
            return sharpenImage;
        }

#region control display
        private void setCounter(int c_number,Label label_object_number,Label label_object_lastTime)
        {
            if (label_object_number == null) { return; }

            int lb_number = int.Parse(label_object_number.Text);
         
            if (c_number != 0)
            {
                lb_number = lb_number + c_number;

                if (lb_number < 0) { lb_number = 0; }
            }
            else { lb_number++; }

            label_object_number.Text = lb_number.ToString();

            ///////update last time
            
            if (label_object_lastTime == null) { return; }

            string DT_now = DateTime.Now.ToString("MM/dd/yy HH:mm:ss"); //.ToOADate();
            label_object_lastTime.Text = DT_now;                   
        }

        private void RenderQrCode()
        {

            string Level_QRCode = Program.QRconfig.level_QRCode;//"L"; //L M Q H;
            int iconSize = Program.QRconfig.iconSize;//24;
            string level = Level_QRCode;
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(label_textQRCode.Text, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {

                        pictureBoxQRCode.BackgroundImage = qrCode.GetGraphic(20, System.Drawing.Color.Black, System.Drawing.Color.White, //default 20
                            GetIconBitmap(), (int)iconSize);

                        this.pictureBoxQRCode.Size = new System.Drawing.Size(pictureBoxQRCode.Width, pictureBoxQRCode.Height);
                        //Set the SizeMode to center the image.
                        this.pictureBoxQRCode.SizeMode = PictureBoxSizeMode.CenterImage;

                        pictureBoxQRCode.SizeMode = PictureBoxSizeMode.StretchImage;
                    }
                }
            }
        }

        private Bitmap GetIconBitmap()
        {
            string iconPath = @Program.QRconfig.iconPath; //"D:\\project C#\\QRCoder-master\\QRCoderDemo\\bin\\Debug\\icon_img\\images.jpg";

            Bitmap img = null;
            if (iconPath.Length > 0)
            {
                try
                {
                    img = new Bitmap(iconPath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    event_logger_detect(ex.Message.ToString());
                }
            }
            return img;
        }

        private void display_on_label()
        {
            double OADate_now = DateTime.Now.ToOADate();//ToString("MM/dd/yyyy HH:mm:ss");
            string result_display_str = "";
            result_display_str += OADate_now.ToString() + "\n";
            result_display_str += textBox1.Text.Trim() + "\n";
            result_display_str += textBox2.Text.Trim() + "\n";
            result_display_str += textBox3.Text.Trim() + "\n";
            result_display_str += textBox4.Text.Trim() + "\n";
            result_display_str += textBox5.Text.Trim();
            label_textOutput.Text = result_display_str.Trim();
            
        }

        private void event_logger_detect(string ex)
        {

            //////////////////////////////////////////write log////////////////////////////////////
            DateTime dt_now = DateTime.Now;
            //string newLine_result_txt = "";
            if (ex == null)
            {
                Program.newLine_result_txt = dt_now.ToString("MM/dd/yyyy HH:mm:ss") +
                                     "," + "\t" + dt_now.ToString("yyyyMMddHHmmss") +
                                     "," + textBox1.Text +
                                     "," + textBox2.Text +
                                     "," + textBox3.Text +
                                     "," + textBox4.Text +
                                     "," + textBox5.Text + "\r\n"; ///เลื่อน curser ไปที่จุดเริ่มต้น \r จากนั้น ขึ้นบรรทัดใหม่ \n
            }
            else
            {
                Program.newLine_result_txt = dt_now.ToString("MM/dd/yyyy HH:mm:ss") +
                                     "," + ex + "\r\n";
            }
            check_loggerWriteLine_exist(Program.newLine_result_txt);
        }
   
        private void clear_data_afterComplete()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();

            textBox1.Focus();
        }

        private void reset_data_instance()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();

            label_error_number.Text = "0";
            label_submit_number.Text = "0";
            label_sync_number.Text = "0";

            label_textOutput.Text = ".....";
            pictureBoxQRCode.Image = null;
            label_textQRCode.Text = ".....";
            label_cache_table_log.Text = ".....";

            textBox1.Focus();
        }

#endregion control display

        private void button_gen_data_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            int n = r.Next();        //สุมตัวเลข
            double left_number = n / 100000484;
            string left_number_str = left_number.ToString("n3");//เอาเลขตัวซ้าย
            ////////////////////////////////////////////////////
            int range_text_space = 12;
            int remaie_left_n = range_text_space - (left_number_str.Length + 3); //" kg" == 3 chaeacter 

            //////add space logical on
            string space_left = "";
            for(var i = remaie_left_n; i > 0; i--) { space_left += " "; }

            textBox1.Text = space_left + left_number_str + " kg";
            textBox2.Text = "     0.34 g/pcs"; //char(15)
            textBox3.Text = "      117 pcs"; //char(13)
            textBox3.Focus();
            //SendKeys.Send("{ENTER}");
            //MessageBox.Show(left_number_str);
            //display_on_label();
            //textBox3.Focus(); //clear_data_afterComplete(); //MessageBox.Show(left_number_str);
        }

        private void button_clear_data_Click(object sender, EventArgs e)
        {
            clear_data_afterComplete();
            resetState_routine(":");
            checkState_routineLogDisplay();
        }

        private void button_execute_print_Click(object sender, EventArgs e)
        {
            printDocument1.Print();//input data to db
            textBox1.Focus();
        }

        private void button_synchronize_Click(object sender, EventArgs e)
        {
            object[] data_row = { "15:00-23:05", true, DateTime.Now.ToOADate() };
            updateState_routine(data_row);
            checkState_routineLogDisplay();

            //forceCreateDBF_existFile();
            //execute_DBFdata_existFile(false);////check exist table. (boolean)bool_syncTemp(spreadsheet)
            //read from db to sync on cloude
            textBox1.Focus();
        }

#region connection dbf
        public static OleDbConnection strConLog;
        DataTable dtData, dtColumn;
        //private DataSet myDataSet;
        //string r = "D:\\DBF_Files";
        //string fileName = "QRcode_weight";
        //string rfileName = @"D:\DBF_Files\QRconfigode_weight.dbf";
        string conString = "Data Source=.; Initial Catalog=myDataBase; Integrated Security=SSPI";

        internal class dbf_field
        {
            public string column0 = "OAStamp_Id";
            public string column1 = "Package_Id";
            public string column2 = "Company_Id";
            public string column3 = "Type_Str";
            public string column4 = "Weight_Str";
            public string column5 = "rMacNo_Str";
            public string column6 = "dStamp_Id";
            public string column7 = "QR_IMG_ID";
        }

        public IList<DbfFieldDescriptor> FieldDescriptors { get; set; }

        private void OleDbConnection_strConLog()
        {
            string query_connection = Program.dbfPath + "\\" + Program.dbfName + ".DBF"; //r + "\\" + fileName + ".dbf";
            string filePath = Path.GetDirectoryName(@query_connection);
            OleDbConnection connection = new OleDbConnection("Provider=VFPOLEDB.1;Data Source=" + filePath + ";");  //Provider=Microsoft.ACE.OLEDB.12.0; || Provider=VFPOLEDB.1;Data Source=
            connection.Open();

            strConLog = connection;
        }

        private void forceCreateDBF_existFile()
        {
            /*
            string query_connection = "Provider=VFPOLEDB.1;Data Source=" + @r;
            string connectionString = query_connection;
            using (OleDbConnection connection = new OleDbConnection(connectionString))
            */
            try
            {
                OleDbConnection_strConLog();
                OleDbConnection connection = strConLog; //Inheritance
                using (OleDbCommand command = connection.CreateCommand())
                {

                    dbf_field _dbf_field = new dbf_field();
                    string query_field_str = String.Format("CREATE TABLE " + Program.dbfName + "({0} F(14,10), {1} C(30), {2} C(30) ,{3} C(30),{4} C(30),{5} C(30), {6} C(30), {7} c(250))",
                        _dbf_field.column0,
                        _dbf_field.column1,
                        _dbf_field.column2,
                        _dbf_field.column3,
                        _dbf_field.column4,
                        _dbf_field.column5,
                        _dbf_field.column6,
                        _dbf_field.column7);

                    //words Prohibited : Device , Code , Equipment , Machine , DateTime ,dt ,Time
                    //connection.Open();//Package_Id = "", Company_Id = "", Type_Str = "", Weight_Str = "", rMacNo_Str = "",DateTime_Id = "",DateTime_Formate = ""
                    string query_create = query_field_str; //"CREATE TABLE " + Program.dbfName + " (OAStamp_Id F(14,10) , Package_Id C(30), Company_Id C(30), Type_Str C(30), Weight_Str C(30), rMacNo_Str C(30), dStamp_Id C(30), QR_IMG_ID c(100))";
                    ////old example //@"CREATE TABLE QRconfigode_weight (Id I, Changed D, Name C(100))"
                    OleDbParameter script = new OleDbParameter("script", query_create);

                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "ExecScript";
                    command.Parameters.Add(script);
                    command.ExecuteNonQuery();
                    stateConnection("new data table");

                }
                connection.Close();
            }
            catch (Exception ex) { stateConnection(ex.Message.ToString()); event_logger_detect(ex.Message.ToString()); }
        }

        private void execute_DBFdata_existFile(Boolean bool_syncTemp)
        {
            OleDbConnection_strConLog();
            OleDbConnection connection = strConLog; //Inheritance

            DataTable tables = connection.GetSchema(OleDbMetaDataCollectionNames.Tables);
            dtColumn = null;
            //string fName = Path.GetFileNameWithoutExtension(@query_connection);

            try
            {
                foreach (DataRow rowTables in tables.Rows)
                {
                    if (rowTables["table_name"].ToString().ToUpper() == Program.dbfName.ToUpper())
                    {
                        string table_db_name = rowTables["table_name"].ToString();
                        DataTable columns = connection.GetSchema(OleDbMetaDataCollectionNames.Columns,
                            new String[] { null, null, rowTables["table_name"].ToString(), null });

                        dtColumn = GetColumnDataTable();
                        foreach (System.Data.DataRow rowColumns in columns.Rows)
                        {
                            DataRow dr = dtColumn.NewRow();
                            dr[0] = rowColumns["column_name"].ToString();
                            dr[1] = OleDbType(int.Parse(rowColumns["data_type"].ToString()));
                            dr[2] = rowColumns["data_type"].ToString();
                            dr[3] = rowColumns["numeric_precision"].ToString();
                            dtColumn.Rows.Add(dr);
                        }
                        break;
                    }
                }

                string sql = "SELECT * FROM " + Program.dbfName;
                OleDbCommand cmd = new OleDbCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                dtData = new DataTable();
                //Make a DataSet object for keep multi dataTable.
                //myDataSet = new DataSet();///maybe not use.
                //Create new List.
                List<IList<Object>> dtData_lines = new List<IList<Object>>();

                OleDbDataAdapter DA = new OleDbDataAdapter(cmd);
                DA.SelectCommand = cmd;
                //DA.Fill(myDataSet, "somename");
                DA.Fill(dtData);

                int dtCount_x = dtData.Rows.Count;
                var dbTable_x = dtData.Rows;

                for (int qw = 0; qw < dtCount_x; qw++)
                {
                    var xxx = dbTable_x[qw].ItemArray[1].ToString().Trim();
                    if(xxx == "PW2020111510233602")//PW2020111510233602 //PW2020030412502201
                    {
                        var qqqq = "";
                    }
                }

#region check get db last from dbf.
                ///////////////////////////////////////////////////////////////////////////////////////mode bool_syncTemp
                if (bool_syncTemp)
                {
                    int index_library_gs1 = 7;
                    int index_change_char = 4;
                    int index_insert_e_char = 3; ////set default.

                    int index_for_find_change_char = -1;

                    int dtCount = dtData.Rows.Count;
                    var dbTable = dtData.Rows;
                    check_readLine_exist();///get last from temp on Program.txtLastSync isNull
                    int txtLastLine_temp = int.Parse(Program.txtLastLine);
                    //int txtLastSync_temp = int.Parse(Program.txtLastSync);        
                    for (int i = 0; i < dtCount; i++)
                    {
                        if (txtLastLine_temp >= i) { continue; } ////Suspend until the next row
                        var dataRow = dbTable[i].ItemArray; ////ทำ array เก็บ แถว ในรูป Table.

                        //////length of row
                        string[] library_gs1_split = dataRow[index_library_gs1].ToString().Split('(');
                        //[j == 7] (21)PW2019092406264101(01)8859229399998(91)PACKAGE(3303)003530(240)WEIGHTSCALE01(8008)190924062641
                        List<Object> range_column = new List<Object>();
                        int dr_length = dataRow.Length;
                        for (int j = 0; j < dr_length; j++)
                        {
                            if (j == index_change_char)
                            {
                                string dataRow_nStr = dataRow[j].ToString().TrimEnd();
                                int length_last = dataRow_nStr.Length;
                                //////STEP 1 loop for find index change char
                                for(var g = 0; g < library_gs1_split.Length; g++)
                                {
                                    string[] library_gs1_split_chaVal = library_gs1_split[g].Split(')');
                                    if (library_gs1_split_chaVal[0].ToString() == "") { continue; }
                                    if (library_gs1_split_chaVal[1].ToString() == dataRow_nStr) {
                                        index_for_find_change_char = g; }
                                }
                                //////STEP 2 set index change char on find value
                                string[] char_vL = library_gs1_split[index_for_find_change_char].ToString().Split(')');
                                try {
                                    index_insert_e_char = int.Parse(char_vL[0].ToString().Substring(2, 2));
                                } catch (Exception ex){
                                    event_logger_detect(ex.Message.ToString());
                                }
                                //(3303) [dr_length-1]
                                if (index_insert_e_char > 0){ dataRow_nStr = dataRow_nStr.Insert(index_insert_e_char, "."); }
                                range_column.Add(dataRow_nStr);
                            }
                            else
                            {
                                range_column.Add(dataRow[j].ToString().Trim());
                            }
                        }
                        dtData_lines.Add(range_column);
                       
                        Program.txtLastSync = i.ToString();
                    }


#region check row last update on text  && sync db on sheet cloud.   
                    //MessageBox.Show(Program.txtLastSync + " <> " + Program.txtLastLine);
                    try
                    {
                        //////////////////////////////////////////////////////////////////////check state netWork_Available ?
                        //bool Net_connection = NetworkInterface.GetIsNetworkAvailable();
                        //Program.netWork_available_bool = Net_connection;
                        check_state_network_available();

                        if (Program.netWork_available_bool && dtData_lines.Count > 0) //////////check netwotk available && dtData_lines > 0
                        {
                            //AuthorizeGoogleApp_getDataPrint();
                            AuthorizeGoogleApp_sync(dtData_lines);
                            label_state_cloud.Text = "sync complete dtData line up (" + dtData_lines.Count + ")";
                            label_state_cloud.ForeColor = System.Drawing.Color.Black;
                            label_state_cloud.BackColor = System.Drawing.Color.LimeGreen;

                            if (updated_state_sync)///update complete
                            {
                                check_readLine_exist();///save last row  : (Program.txtLastSync) value != 0.
                                setCounter((1), label_sync_number, label_sync_lastTime);
                            }
                            else
                            {
                                label_state_cloud.Text = "sheet synchronize rows failed on dtData line up (" + dtData_lines.Count + ")";
                                label_state_cloud.ForeColor = System.Drawing.Color.White;
                                label_state_cloud.BackColor = System.Drawing.Color.OrangeRed;

                                setCounter((1), label_error_number, null);
                            }
                        }
                        else
                        {
                            label_state_cloud.Text = "netWork available (" + Program.netWork_available_bool.ToString() + ") && dtData line up (" + dtData_lines.Count + ")";
                            label_state_cloud.ForeColor = System.Drawing.Color.White;
                            label_state_cloud.BackColor = System.Drawing.Color.OrangeRed;

                            setCounter((1), label_error_number, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        label_state_cloud.Text = ex.Message.ToString();
                        label_state_cloud.ForeColor = System.Drawing.Color.White;
                        label_state_cloud.BackColor = System.Drawing.Color.Red;

                        event_logger_detect(ex.Message.ToString());
                    }
                  
#endregion check row last update on text  && sync db on sheet cloud 
                }
#endregion check get db last from dbf. 
                ///////////////////////////////////////////////////////////////////////////////////////mode bool_syncTemp

                /////////update last line sync to sheet
                stateConnection("con");
            }
            catch (Exception ex) {
                stateConnection(ex.Message.ToString()); event_logger_detect(ex.Message.ToString()); }

            ///////////////////////////////////////////////////////////////insert execution (insert,delete,clean)
            connection.Close();
            ///////////////////////////clear txtLastSync for protect replace value after error
            Program.txtLastSync = "";
        }

#region sync sheet on cloud0   

        static bool updated_state_sync = false;

        //ref credential https://developers.google.com/sheets/api/quickstart/dotnet (ใช้ได้เลย ตั้ง path credentials.json ที่ได้ เก็บไว้ใน instant execution(build))
        //ref example https://console.developers.google.com/flows/enableapi?apiid=sheets.googleapis.com (ตั้งค่าไว้ก่อน แต่ยังไม่ได้ใช้)
        static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "QRCoderDemo25052019";//"TimeSheetUpdation By Cybria Technology";

        private static SheetsService AuthorizeGoogleApp_sync(List<IList<Object>> table_db_sync)
        {
            
            Console.WriteLine("HELLO");
            
            UserCredential credential;
            //"credentials.json" || "C:\\Users\\BOYZ23\\Downloads\\QRCoderDemo25052019-dfc023719abf.json"
            
            using (var stream =
                new FileStream("gg_credential\\credentials.json", FileMode.Open, FileAccess.Read)) //C:\\Users\\BOYZ23\\Downloads\\QRCoderDemo25052019-dfc023719abf.json
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");//
                 
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;


             
                //MessageBox.Show("Credential file saved to: " + credPath);
                //Console.WriteLine("Credential file saved to: " + credPath);
            }
           
            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

#region library range index
            ////////////check result row is 0
            if (table_db_sync[0].Count <= 0) { return service; }

            List<string> cha_range = new List<string>();
            cha_range.Add("@");
            cha_range.Add("A");
            cha_range.Add("B");
            cha_range.Add("C");
            cha_range.Add("D");
            cha_range.Add("E");
            cha_range.Add("F");
            cha_range.Add("G");
            cha_range.Add("H");
            cha_range.Add("I");
            cha_range.Add("J");
            cha_range.Add("K");
            cha_range.Add("L");
            cha_range.Add("M");
            cha_range.Add("N");
            cha_range.Add("O");
            cha_range.Add("P");
            cha_range.Add("Q");
            cha_range.Add("R");
            cha_range.Add("S");
#endregion library range index

            updated_state_sync = false;
            string end_rCha = cha_range[table_db_sync[0].Count];
            int end_rNum = table_db_sync.Count;

            /////check list dest file is null
            if (Program.QRconfig.syncSheetChr.ToString() == "") { return service; }
            string[] syncSheetChr = Program.QRconfig.syncSheetChr.Split(',');
            foreach (string syncSheet_x in syncSheetChr)
            {
                try
                {
                    string[] syncSheet_x_list = syncSheet_x.Split('(');//range_Time_routine[range_Time_x];
                    string syncSheetChr_id = syncSheet_x_list[0].ToString();
                    string syncSheetChr_nameR = "";
                    try
                    {
                        syncSheetChr_nameR = syncSheet_x_list[1].ToString().Replace(')', ' ').TrimEnd();
                    }catch
                    {
                        syncSheetChr_nameR = Program.QRconfig.syncRange;
                    }


                    // Define request parameters. 
                    String spreadsheetId = syncSheetChr_id;//"1p0kA1C98xO1WEiz281I_8UkufstsOREsOcZzU1T7irU";
                    String range = syncSheetChr_nameR + end_rCha; //"Sheet1!A2:" + Bxx

                    ///////////////////////////////////////////////////get data from sheet/////////////////////////////////////////////////////
                    SpreadsheetsResource.ValuesResource.GetRequest request_bf_update =
                        service.Spreadsheets.Values.Get(spreadsheetId, range);

                    ValueRange response_bf_update = request_bf_update.Execute();
                    IList<IList<Object>> values_bf_update = response_bf_update.Values;

                    UpdatGoogleSheetinBatch(table_db_sync, spreadsheetId, range, service);
               

                    ///////////////////////////////////////////////////get data from sheet/////////////////////////////////////////////////////
                    SpreadsheetsResource.ValuesResource.GetRequest request_aft_update = 
                        service.Spreadsheets.Values.Get(spreadsheetId, range);

                    ValueRange response_aft_update = request_aft_update.Execute();
                    IList<IList<Object>> values_aft_update = response_aft_update.Values;

                    if((values_aft_update.Count - values_bf_update.Count) > 0)
                    { updated_state_sync = true; }

                    // Prints the names and majors of students in a sample spreadsheet:
                    //QRCode25052019_syncData : https://docs.google.com/spreadsheets/d/1p0kA1C98xO1WEiz281I_8UkufstsOREsOcZzU1T7irU/edit#gid=0
            
                    if (values_aft_update != null && values_aft_update.Count > 0)
                    {
                        Console.WriteLine("Name, Major");
                        foreach (var row in values_aft_update)
                        {
                            // Print columns A and E, which correspond to indices 0 and 4.
                            Console.WriteLine("{0}, {1}", row[0], row[4]);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No data found.");
                    }
                    Console.Read();
                }
                catch(Exception ex) { } /////cannot push message alert. because this static method.
            }
            
            return service;
        }

        static bool updated_state_print_getData = false;
        private SheetsService AuthorizeGoogleApp_getDataPrint()
        {
            UserCredential credential; //"credentials.json" || "C:\\Users\\BOYZ23\\Downloads\\QRCoderDemo25052019-dfc023719abf.json"
            using (var stream =
                new FileStream("gg_credential\\credentials.json", FileMode.Open, FileAccess.Read)) //C:\\Users\\BOYZ23\\Downloads\\QRCoderDemo25052019-dfc023719abf.json
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");//

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                //MessageBox.Show("Credential file saved to: " + credPath);
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            /////////protect duplicate process.
            if (updated_state_print_getData == true) { return service; }
            updated_state_print_getData = true;

            //return service;
            /////check list dest file is null
            if (Program.QRconfig.syncSheetChr.ToString() == "") { return service; }
            string[] syncSheetChr = Program.QRconfig.getSheetChr.Split(',');
            foreach (string syncSheet_x in syncSheetChr)
            {
                try
                {
                    string[] syncSheet_x_list = syncSheet_x.Split('(');//range_Time_routine[range_Time_x];
                    string syncSheetChr_id = syncSheet_x_list[0].ToString();
                    string syncSheetChr_nameR = syncSheet_x_list[1].ToString().Replace(')', ' ').TrimEnd();
                    // Define request parameters. 
                    String spreadsheetId = syncSheetChr_id;//"1p0kA1C98xO1WEiz281I_8UkufstsOREsOcZzU1T7irU";
                    String range = syncSheetChr_nameR; //"Sheet1!A2:" + Bxx

                    ///////////////////////////////////////////////////get data from sheet/////////////////////////////////////////////////////
                    SpreadsheetsResource.ValuesResource.GetRequest request_bf_update =
                        service.Spreadsheets.Values.Get(spreadsheetId, range);

                    ValueRange response_bf_update = request_bf_update.Execute();
                    IList<IList<Object>> values_bf_update = response_bf_update.Values;

                    //UpdatGoogleSheetinBatch(table_db_sync, spreadsheetId, range, service);
                    for(var row_i = 0; row_i < values_bf_update.Count; row_i++) {

                        if (values_bf_update[row_i].Count < 11) { continue; }

                        var barcode_id = values_bf_update[row_i][0];
                        var sheet_weight_str = values_bf_update[row_i][3];
                        var sheet_bagNo_str = values_bf_update[row_i][6];
                        var sheet_grade_str = values_bf_update[row_i][7];
                        var sheet_serial_str = values_bf_update[row_i][8];
                        var state_print = values_bf_update[row_i][9];
                        var sheet_print_now_str = values_bf_update[row_i][10];

                        if (barcode_id.ToString() == "" || (state_print.ToString() != "" && barcode_id.ToString() != "") || sheet_print_now_str.ToString() == "") { continue; }

                        ////convert to oadate full 
                        string full_date_time_str = sheet_serial_str.ToString();
                        for (var cL = 14; full_date_time_str.Length < cL;) { full_date_time_str = full_date_time_str + "0"; }
                        //14
                        /*string year_str = full_date_time_str.Substring(0, 4);
                        string month_str = full_date_time_str.Substring(4, 2);
                        string day_str = full_date_time_str.Substring(6, 2);
                        string hour_str = full_date_time_str.Substring(8, 2);
                        string minute_str = full_date_time_str.Substring(10, 2);
                        string secound_str = full_date_time_str.Substring(12, 2);*/

                        double OADate_now = DateTime.Now.ToOADate(); //ToString("MM/dd/yyyy HH:mm:ss");
                        string result_display_str = "";
                        result_display_str += OADate_now.ToString() + "\n";

                        //string text_0 = sheet_weight_str.ToString() + " kg";
                        //for (var space_c = 12; text_0.Length < space_c;) { text_0 = " " + text_0; }

                        result_display_str += sheet_weight_str.ToString() + " kg" + "\n";
                        result_display_str += sheet_weight_str.ToString() + " g/pcs" + "\n";

                        //text_0 = 1 + " pcs";
                        //for (var space_c = 15; text_0.Length < space_c;) { text_0 = " " + text_0; }

                        result_display_str += 1 + " pcs" + "\n";
                        result_display_str += full_date_time_str + " m_id" + "\n";
                        result_display_str += barcode_id + " barcode_id";
                        label_textOutput.Text = result_display_str.Trim();

                        //"43941.5040503588\n19.000 kg\n0.34 g/pcs\n117 pcs"
                        ////convert tp weight full

                        //.....Program.GS1_data.Weight_Str = 
                        //.....Program.DBF_data.Weight_Str = weight_str.ToString();
                        //.....Program.DBF_data.Package_Id = text_display_id.ToString();

                        render_data_print_save(result_display_str);


                        /////update on old array get nothing
                        //values_bf_update[row_i][2] = formate_dd_MM_yyyy;

                        //////update time
                        string[] syncSheetChr_nameR_split = syncSheetChr_nameR.Split('!');
                        string range_update = syncSheetChr_nameR_split[0] + "!A" + ((int.Parse(row_i.ToString()))+2).ToString();

                        String range2 = range_update;  // update cell F5 
                        ValueRange valueRange = new ValueRange();
                        valueRange.MajorDimension = "COLUMNS";//"ROWS";//COLUMNS

                        var oblist = new List<object>() { formate_dd_MM_yyyy };
                        valueRange.Values = new List<IList<object>> { oblist };

                        SpreadsheetsResource.ValuesResource.UpdateRequest update = service.Spreadsheets.Values.Update(valueRange, spreadsheetId, range2);
                        update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
                        UpdateValuesResponse result2 = update.Execute();

                        //updated_state_print_getData = true;
                        Console.WriteLine("done!");

                        /*
                        String spreadsheetId2 = "<my spreadsheet ID>";
                        String range2 = "<my page name>!F5";  // update cell F5 
                        ValueRange valueRange = new ValueRange();
                        valueRange.MajorDimension = "COLUMNS";//"ROWS";//COLUMNS

                        var oblist = new List<object>() { "My Cell Text" };
                        valueRange.Values = new List<IList<object>> { oblist };

                        SpreadsheetsResource.ValuesResource.UpdateRequest update = service.Spreadsheets.Values.Update(valueRange, spreadsheetId2, range2);
                        update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
                        UpdateValuesResponse result2 = update.Execute();

                        Console.WriteLine("done!");
                        */

                        // Prints the names and majors of students in a sample spreadsheet:
                        //IList<IList<Object>> values = response.Values;
                        //Console.WriteLine(values);
                    }


                    ///////////////////////////////////////////////////get data from sheet/////////////////////////////////////////////////////
                    /*
                    SpreadsheetsResource.ValuesResource.GetRequest request_aft_update =
                        service.Spreadsheets.Values.Get(spreadsheetId, range);

                    ValueRange response_aft_update = request_aft_update.Execute();
                    IList<IList<Object>> values_aft_update = response_aft_update.Values;

                    if ((values_aft_update.Count - values_bf_update.Count) > 0)
                    { updated_state_print_getData = true; }
                    

                    // Prints the names and majors of students in a sample spreadsheet:
                    //QRCode25052019_syncData : https://docs.google.com/spreadsheets/d/1F0uDc1ezDLrQrQnxVYvAyXXUsGkuuPwMyTu-yIRLbDs/edit#gid=0

                    if (values_aft_update != null && values_aft_update.Count > 0)
                    {
                        Console.WriteLine("Name, Major");
                        foreach (var row in values_aft_update)
                        {
                            // Print columns A and E, which correspond to indices 0 and 4.
                            Console.WriteLine("{0}, {1}", row[0], row[4]);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No data found.");
                    }
                    Console.Read();
                    */
                }
                catch (Exception ex) { } /////cannot push message alert. because this static method.
            }
            /////////protect duplicate process. unlock
            updated_state_print_getData = false;
            return service;
        }

        private void ReadLogBackup()
        {
            var fileStream = new FileStream(Program.QRconfig.txtLoggerPath, FileMode.Open, FileAccess.Read); //@"c:\file.txt"
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                bool start_now = false;
                string line_replace_text = "",line;
                string[] line_split;
                int row=0;
                while ((line = streamReader.ReadLine()) != null)
                {
                    row++;
                    if (line.IndexOf("has become corrupted") > -1) {
                        var x = "";
                        start_now = true;
                        continue;
                    }
                    if (start_now) {
                        
                        line_split = line.Split(',');
                        if (line_split.Length != 7) { continue; }

                        for (var i = 0; i <= line_split.Length - 1; i++) {

                            if (line_split[i].IndexOf(":") > -1 && line_split[i].IndexOf("/") > -1) {

                                string[] date_time_array = line_split[i].Split(' ');
                                ///date_str_array : formate mm/dd/yyyy HH:mm:ss
                                string[] date_str_array = date_time_array[0].Split('/');
                                string[] time_str_array = date_time_array[1].Split(':');

                                int str_Year = Int32.Parse(date_str_array[2]);
                                int str_Month = Int32.Parse(date_str_array[0]);
                                int str_Day = Int32.Parse(date_str_array[1]);
                                int str_HH = Int32.Parse(time_str_array[0]);
                                int str_mm = Int32.Parse(time_str_array[1]);
                                int str_ss = Int32.Parse(time_str_array[2]);
                                //new DateTime(DT_now.Year, DT_now.Month, DT_now.Day, point_n_st0, point_n_st1, point_n_st2)
                                DateTime DT_range_st = new DateTime(str_Year, str_Month, str_Day, str_HH, str_mm, str_ss);
                                double OA_range_st = DT_range_st.ToOADate();
                                line_split[i] = OA_range_st.ToString();
                            }
                            line_replace_text += line_split[i].Replace("\t", "") + "\n";                           
                        }
                        render_data_print_save(line_replace_text);
                        line_replace_text = "";
                        ///formate date mm-dd-yyyy
                        var h = "";
                        
                    }
                    // process the line
                    //render_data_print_save("");
                }
            }
        }

        private static void UpdatGoogleSheetinBatch(IList<IList<Object>> values, string spreadsheetId, string newRange, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.AppendRequest request =
               service.Spreadsheets.Values.Append(new ValueRange() { Values = values }, spreadsheetId, newRange);
            request.InsertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            var response = request.Execute();
        }
#endregion sync sheet on cloud0

        private void insertDBF_row()
        {
            execute_DBFdata_existFile(false);////check exist table. (boolean)bool_syncTemp(spreadsheet)

            OleDbConnection_strConLog();
            OleDbConnection connection = strConLog; //Inheritance


            dbf_field _dbf_field = new dbf_field();
            string query_field_str = String.Format("Insert into " + Program.dbfName + "({0}, {1}, {2} ,{3} ,{4} ,{5}, {6}, {7})",
                _dbf_field.column0,
                _dbf_field.column1,
                _dbf_field.column2,
                _dbf_field.column3,
                _dbf_field.column4,
                _dbf_field.column5,
                _dbf_field.column6,
                _dbf_field.column7);
            string query_value_str = String.Format("values( {0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}')", /////is integer || double || float  {0} wich out "'"
                Program.DBF_data.OAStamp_Id,
                Program.DBF_data.Package_Id,
                Program.DBF_data.Company_Id,
                Program.DBF_data.Type_Str,
                Program.DBF_data.Weight_Str,
                Program.DBF_data.rMacNo_Str,
                Program.DBF_data.dStamp_Id,
                Program.DBF_data.QR_IMG_ID);

            try
            {
                OleDbCommand oComm = new OleDbCommand(query_field_str + query_value_str, strConLog);
                oComm.ExecuteNonQuery();
                //execute_DBFdata_existFile(false);////check exist table. (boolean)bool_syncTemp(spreadsheet)
            }
            catch (Exception ex) { stateConnection(ex.Message.ToString()); event_logger_detect(ex.Message.ToString()); }
        }

        private void stateConnection(String ex)
        {

            if (ex == "") { label_state_connDB.Text = "nothing"; label_state_connDB.BackColor = System.Drawing.Color.Gray; }
            else if (ex.ToLower().IndexOf("con") > -1)
            {
                label_state_connDB.Text = "connected";
                label_state_connDB.BackColor = System.Drawing.Color.LimeGreen;
                label_state_connDB.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                label_state_connDB.Text = "" + ex;
                label_state_connDB.BackColor = System.Drawing.Color.Red;
                label_state_connDB.ForeColor = System.Drawing.Color.White;
            }

            if (ex.ToLower().IndexOf("exist") > -1)
            {
                label_state_connDB.Text = ex;
                label_state_connDB.BackColor = System.Drawing.Color.Yellow;
                label_state_connDB.ForeColor = System.Drawing.Color.Black;
                forceCreateDBF_existFile();
                /////force new data table.
            }
        }

        // In this method we have create the table that will be stored the columns name and datatype.  
        static DataTable GetColumnDataTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("NAME", typeof(string));
            table.Columns.Add("TYPE", typeof(string));
            table.Columns.Add("TYPENO", typeof(string));
            table.Columns.Add("DEC", typeof(string));
            return table;
        }

        public class DbfFieldDescriptor
        {
            public int No { get; set; }
            public string Name { get; set; }
            public char TypeChar { get; set; }
            public int Length { get; set; }
            public byte DecimalCount { get; set; }

            public string GetSqlDataType()
            {
                switch (TypeChar)
                {
                    case 'C':
                        return $"VARCHAR({Length})";
                    case 'I':
                        return "INT";
                    case 'N':
                        return $"DECIMAL({Length + 1}, {DecimalCount})";
                    case 'L':
                        return "BIT";
                    case 'B':
                        return "BIT";
                    case 'D':
                        return "DATETIME";
                    case 'M':
                        return "VARCHAR(MAX)";
                    case 'S':
                        return "VARCHAR(MAX)";
                    case 'T':
                        return "DATETIME";
                    case 'W': //?  
                        return "VARCHAR(MAX)";
                    case '0':
                        return "INT";
                    case 'G':
                        return "VARCHAR(MAX)";
                    case 'F':
                        return "FLOAT";
                    case 'Y':
                        return "NUMERIC(18,4)";
                    default:
                        throw new NotSupportedException();
                }
            }
        }

        // This method return the datatype name.  
        public string OleDbType(int type)
        {
            string dataType;
            switch (type)
            {
                case 10:
                    dataType = "BigInt";
                    break;
                case 128:
                    dataType = "Byte";
                    break;
                case 11:
                    dataType = "Boolean";
                    break;
                case 8:
                    dataType = "String";
                    break;
                case 129:
                    dataType = "String";
                    break;
                case 6:
                    dataType = "Currency";
                    break;
                case 7:
                    dataType = "DateTime";
                    break;
                case 133:
                    dataType = "DateTime";
                    break;
                case 134:
                    dataType = "TimeSpan";
                    break;
                case 135:
                    dataType = "DateTime";
                    break;
                case 14:
                    dataType = "Decimal";
                    break;
                case 5:
                    dataType = "Double";
                    break;
                case 3:
                    dataType = "Integer";
                    break;
                case 201:
                    dataType = "String";
                    break;
                case 203:
                    dataType = "String";
                    break;
                case 204:
                    dataType = "Byte";
                    break;
                case 200:
                    dataType = "String";
                    break;
                case 139:
                    dataType = "Decimal";
                    break;
                case 202:
                    dataType = "String";
                    break;
                case 130:
                    dataType = "String";
                    break;
                case 131:
                    dataType = "Decimal";
                    break;
                case 64:
                    dataType = "DateTime";
                    break;

                default:
                    dataType = "";
                    break;
            }

            return dataType;
        }

        public void WriteDataToDatabase(DataTable dtData, DataTable dtCol)
        {
            string query_connection = Program.dbfPath + "\\" + Program.dbfName + ".dbf";  // r + "\\" + fileName + ".dbf";
            string TableName = Path.GetFileNameWithoutExtension(@query_connection);
            string FilePath = @query_connection;
            SqlConnection dbCon = new SqlConnection(conString);
            if (dbCon.State == ConnectionState.Closed)
                dbCon.Open();
            string strQuery = "IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'" + TableName + "')) BEGIN SELECT 1 END ELSE BEGIN SELECT 0 END";
            SqlCommand dbCmd = new SqlCommand(strQuery, dbCon);
            SqlDataAdapter dbDa = new SqlDataAdapter(dbCmd);
            DataTable dtExist = new DataTable();
            dbDa.Fill(dtExist);
            int valReturn = int.Parse(dtExist.Rows[0][0].ToString());
            if (valReturn == 0)
            {
                ReadFileStream(FilePath, dtCol);
                CreateDbTable(TableName);
            }

            saveToDb(dtData, TableName, dtCol);
        }

        // this method will create table in the database.  
        void CreateDbTable(string TableName)
        {
            try
            {
                SqlConnection dbCon = new SqlConnection(conString);
                if (dbCon.State == ConnectionState.Closed)
                    dbCon.Open();
                using (SqlCommand cmd = dbCon.CreateCommand())
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine($"CREATE TABLE [{TableName}] (");
                    bool first = true;
                    foreach (var fieldDescriptor in FieldDescriptors)
                    {
                        if (first)
                            first = false;
                        else
                            sb.Append(", ");
                        sb.AppendLine($"[{fieldDescriptor.Name}] {fieldDescriptor.GetSqlDataType()}");
                    }
                    sb.Append(", ");
                    sb.AppendLine($"[{"lactivestatus"}] {"bit"}");
                    sb.AppendLine($")");
                    cmd.CommandText = sb.ToString();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Failed to create table {TableName}", e);
            }
        }

        // Here we save records to the database.  
        public void saveToDb(DataTable dtOne, string TableName, DataTable dtColumn)
        {
            if (dtOne.Rows.Count > 0)
            {
                SqlConnection dbCon = new SqlConnection(conString);
                for (int n = 0; n < dtOne.Rows.Count; n++)
                {
                    if (dbCon.State == ConnectionState.Closed)
                        dbCon.Open();

                    string strQry = "";
                    strQry = "INSERT INTO [" + TableName + "] VALUES(";
                    for (int i = 0; i < dtColumn.Rows.Count; i++)
                    {
                        if (i == dtColumn.Rows.Count - 1)
                        {
                            if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "boolean" || dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "logical")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'False','true')";
                                else
                                    strQry = strQry + "'" + dtOne.Rows[n][i].ToString() + "','true')";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "string")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'','true')";
                                else
                                    strQry = strQry + "'" + dtOne.Rows[n][i].ToString().Replace("'", "") + "','true')";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "byte")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'0','true')";
                                else
                                    strQry = strQry + "'" + Encoding.ASCII.GetBytes(dtOne.Rows[n][i].ToString()) + "','true')";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "character")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'','true')";
                                else
                                    strQry = strQry + "'" + dtOne.Rows[n][i].ToString().Replace("'", "") + "','true')";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "datetime" || dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "date")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "null,'true')";
                                else
                                    strQry = strQry + "'" + DateTime.Parse(dtOne.Rows[n][i].ToString()) + "','true')";
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "0,'true')";
                                else
                                    strQry = strQry + dtOne.Rows[n][i].ToString() + ",'true')";
                            }
                        }
                        else
                        {
                            if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "boolean" || dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "logical")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'False',";
                                else
                                    strQry = strQry + "'" + dtOne.Rows[n][i].ToString() + "',";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "string")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'',";
                                else
                                    strQry = strQry + "'" + dtOne.Rows[n][i].ToString().Replace("'", "") + "',";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "byte")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'0',";
                                else
                                    strQry = strQry + "'" + Encoding.ASCII.GetBytes(dtOne.Rows[n][i].ToString()) + "',";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "character")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "'',";
                                else
                                    strQry = strQry + "'" + dtOne.Rows[n][i].ToString().Replace("'", "") + "',";
                            }
                            else if (dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "datetime" || dtColumn.Rows[i]["TYPE"].ToString().ToLower() == "date")
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "null,";
                                else
                                    strQry = strQry + "'" + DateTime.Parse(dtOne.Rows[n][i].ToString()) + "',";
                            }
                            else
                            {
                                if (string.IsNullOrWhiteSpace(dtOne.Rows[n][i].ToString()))
                                    strQry = strQry + "0,";
                                else
                                    strQry = strQry + dtOne.Rows[n][i].ToString() + ",";
                            }
                        }
                    }

                    try
                    {
                        SqlCommand dbCmd1 = new SqlCommand(strQry, dbCon);
                        dbCmd1.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error " + ex.Message);
                    }
                }
                dbCon.Close();
            }
        }

        void ReadFileStream(string FilePath, DataTable dtCol)
        {
            FileStream fileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);

            var fieldDescriptors = new List<DbfFieldDescriptor>();
            try
            {
                int no = 0;
                while (true)
                {
                    var fieldDescriptor = ReadFieldDescriptor(binaryReader, no++, dtCol);
                    if (fieldDescriptor == null)
                        break;
                    if (no > 1)
                        fieldDescriptors.Add(fieldDescriptor);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Failed to read field descriptors", e);
            }
            FieldDescriptors = fieldDescriptors;
        }

        DbfFieldDescriptor ReadFieldDescriptor(BinaryReader br, int fdNo, DataTable dtCol)
        {
            var fieldDescriptor = new DbfFieldDescriptor();
            fieldDescriptor.No = fdNo;
            string name = "";
            if (fdNo > 0 && fdNo <= dtCol.Rows.Count)
                name = dtCol.Rows[fdNo - 1][0].ToString();
            try
            {
                var fieldNameBytes = new byte[11];
                fieldNameBytes[0] = br.ReadByte();
                if (fieldNameBytes[0] == 0x0D)
                    return null; // 0x0D means end of field descriptor list  

                br.Read(fieldNameBytes, 1, 10);
                fieldDescriptor.Name = name;
                fieldDescriptor.TypeChar = (char)br.ReadByte();
                br.ReadByte(); // reserved  
                br.ReadByte(); // reserved  
                br.ReadByte(); // reserved  
                br.ReadByte(); // reserved  
                fieldDescriptor.Length = br.ReadByte();
                fieldDescriptor.DecimalCount = br.ReadByte();
                br.ReadBytes(2); // work area id  
                br.ReadByte(); // example  
                br.ReadBytes(10); // reserved  
                br.ReadByte(); // production mdx  

                return fieldDescriptor;
            }
            catch (Exception e)
            {
                if (string.IsNullOrWhiteSpace(fieldDescriptor.Name))
                    throw new Exception($"Failed to read field descriptor #{fdNo + 1}", e);
                else
                    throw new Exception($"Failed to read field descriptor #{fdNo + 1} ({fieldDescriptor.Name})", e);
            }
        }
#endregion connection dbf

#region readWriteTxtFile
        private void check_readLine_exist()
        {
            try
            {
                Program.txtLastLine = "0"; ///test line default

                string path_file = Program.QRconfig.txtReadLinePath;//"D:\\DBF_Files\\dbf_readLine.txt";

                if (!File.Exists(path_file))
                {
                    // Create a file to write to.
                    using (StreamWriter sw_f = File.CreateText(path_file))
                    {
                        sw_f.WriteLine("0");
                    }
                }

#region find value continue in logger.
                //Create new List.
                List<string> lines = new List<string>();
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(path_file);

                //simulate value on pattern in while loop.
                string lineAt;
                while ((lineAt = sr.ReadLine()) != null)
                {
                    // Insert logic here.
                    // ... The "line" variable is a line in the file.
                    // ... Add it to our List.
                    lines.Add(lineAt);
                }

                //update last line temp from List <string>"lines".
                foreach (string value in lines)
                {
                    if (value != "") { Program.txtLastLine = value; } //Program.txtLastLine = value;
                }

                //close the file before start StreamWriter new
                sr.Close();
#endregion find value continue in logger.

                /////update fileText if (Program.txtLastSync && Program.txtLastLine) value is different and first input.
                if (Program.txtLastSync != Program.txtLastLine && Program.txtLastSync != "")
                {
                    //string[] Lastlines = new string[] { "4", "1" };
                    ///////write new line
                    using (StreamWriter sw_writer = new StreamWriter(path_file))
                    {
                        sw_writer.WriteLine(Program.txtLastSync);
                        sw_writer.Close();
                    }

                    //////inheritance for value present.
                    Program.txtLastLine = Program.txtLastSync;
                    ///clear affter updated.
                    Program.txtLastSync = "";
                }


                Console.ReadKey();
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
                stateConnection("readLine Error : " + e.Message.ToString());
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
                stateConnection("readLine : Executing finally complete");
            }
        }
#endregion readWriteTxtFile

#region readWriteTxtFile
        private void check_loggerWriteLine_exist(string newLine_result_txt)
        {
            try
            {

                string path_file = Program.QRconfig.txtLoggerPath;//"TXT_writeLog\\macro_loggerWrite.txt";

                if (!File.Exists(path_file))
                {
                    // Create a file to write to.
                    using (StreamWriter sw_f = File.CreateText(path_file))
                    {
                        sw_f.WriteLine(newLine_result_txt);
                    }
                }
                //////new line all Text
                else { File.AppendAllText(path_file, newLine_result_txt); }
                
                //add new line.
                //Console.ReadKey();
            }
            catch (Exception e)
            {
                //Console.WriteLine("Exception: " + e.Message);
                stateConnection("logger Write Error : " + e.Message.ToString());
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
                stateConnection("logger Write : Executing finally complete");
            }
        }

        private void button_repair_data_Click(object sender, EventArgs e)
        {
            ReadLogBackup();
        }
#endregion readWriteTxtFile

#region xmlReadWrite
        private string txtReadLine_path = "TXT_readLine\\dbf_readLine.txt";
        private string xmlFile_path = "XML_config\\qrcode_config.xml";
        private string dbfFile_path = "DBF_File\\QRCode_weight.DBF";
        private string txtLogger_path = "TXT_writeLog\\macro_loggerWrite.txt";
        private string rangeTimeChr = "20:00-20:05,01:01-01:05";
        private string syncSheetChr = "1p0kA1C98xO1WEiz281I_8UkufstsOREsOcZzU1T7irU";
        private string syncRange = "Sheet4!A2:";
        private string getSheetChr = "1F0uDc1ezDLrQrQnxVYvAyXXUsGkuuPwMyTu-yIRLbDs(rawMat_page!B2:L1000)"; ///fix update state "!A" 
        private string pictureSizeSlt = "146*146";
        private string txt_xPoint = "9";
        private string txt_yPoint = "142";
        private string txt_sizePost = "7";
        //private string txt_validation_query = "kg:12,g/pcs:15";
        private string txt_validation_query = ",";

        private void parameterDeclareDBF()
        {
            ////split parameter from dbfpath
            Program.dbfPath = "";
            Program.dbfName = "";
            if (Program.QRconfig.dbfPath == null) { Program.QRconfig.dbfPath = dbfFile_path; }

            string[] dbPath_list = Program.QRconfig.dbfPath.Split('\\');

            int dbPath_list_length = dbPath_list.Length;
            //program.dbfPath
            for (int n = 0; n < dbPath_list_length - 1; n++)
            {
                string separator_sym = "\\";
                if (n == (dbPath_list_length - 2)) { separator_sym = ""; }
                Program.dbfPath += (dbPath_list[n] + separator_sym);
            }
            //program.dbfName
            Program.dbfName = dbPath_list[dbPath_list_length - 1].Replace(".dbf", "");
            //MessageBox.Show("Program.dbfPath : " + Program.dbfPath + " " +"Program.dbfName : " + Program.dbfName);
        }

        private void xmlCreateAndWrite_exist()
        {

            //string filePath = Path.GetDirectoryName(Program.QRconfig.xmlPath);
            Program.QRconfig = new QRCode_config();
            ////////////////DEFAULT///////////////
            Program.QRconfig.macNo = 2;
            Program.QRconfig.iconPath = "icon_img\\images.jpg";
            Program.QRconfig.level_QRCode = "L";
            Program.QRconfig.iconSize = 24;
            Program.QRconfig.interval_time = 1000;
            Program.QRconfig.txtReadLinePath = txtReadLine_path;
            Program.QRconfig.xmlPath = xmlFile_path;
            Program.QRconfig.dbfPath = dbfFile_path;
            Program.QRconfig.txtLoggerPath = txtLogger_path;
            Program.QRconfig.rangeTimeChr = rangeTimeChr;
            Program.QRconfig.syncSheetChr = syncSheetChr;
            Program.QRconfig.getSheetChr = getSheetChr;
            Program.QRconfig.maximum_weight_g = 1000;
            Program.QRconfig.minimum_weight_g = 0;
            Program.QRconfig.pictureSizeSlt = pictureSizeSlt;
            Program.QRconfig.txt_xPoint = txt_xPoint;
            Program.QRconfig.txt_yPoint = txt_yPoint;
            Program.QRconfig.txt_sizePost = txt_sizePost;
            Program.QRconfig.txt_validation_query = txt_validation_query;
          

            parameterDeclareDBF();
            FileStream fs = new FileStream(Program.QRconfig.xmlPath, FileMode.Create, FileAccess.Write);
            lq.Add(Program.QRconfig);
            xs.Serialize(fs, lq);
            ///////////close after read every time
            fs.Close();
        }

        private void xmlWriteAndChangedxx_exist()
        {
            ///////////////////////clear!!!!
            lq.Clear();
            FileStream fs = new FileStream(Program.QRconfig.xmlPath, FileMode.Truncate, FileAccess.Write);
            xs.Serialize(fs, lq);
            ///////////close after read every time
            fs.Close();

            Program.QRconfig = new QRCode_config();
            ////////////////DEFAULT///////////////
            Program.QRconfig.macNo = int.Parse(numericUpDown_macNo.Value.ToString());
            Program.QRconfig.iconPath = textBox_iconPath.Text;
            Program.QRconfig.level_QRCode = comboBox_levelQR.Text;
            Program.QRconfig.iconSize = int.Parse(numericUpDown_iconSize.Value.ToString());
            Program.QRconfig.interval_time = int.Parse(numericUpDown_interval_time.Value.ToString());
            Program.QRconfig.txtReadLinePath = textBox_text_readLine.Text;
            Program.QRconfig.xmlPath = textBox_xml_path.Text;
            Program.QRconfig.dbfPath = textBox_dbf_path.Text;
            Program.QRconfig.txtLoggerPath = textBox_logger_path.Text;
            Program.QRconfig.rangeTimeChr = textBox_range_time.Text;
            Program.QRconfig.syncSheetChr = textBox_sync_sheetChr.Text;
            Program.QRconfig.getSheetChr = textBox_get_sheetChr.Text;
            Program.QRconfig.maximum_weight_g = int.Parse(numericUpDown_maximum_weight_g.Value.ToString());
            Program.QRconfig.minimum_weight_g = int.Parse(numericUpDown_minimum_weight_g.Value.ToString());
            Program.QRconfig.pictureSizeSlt = textBox_pictureBox_size.Text;
            Program.QRconfig.txt_xPoint = textBox_set_post_x.Text;
            Program.QRconfig.txt_yPoint = textBox_set_post_y.Text;
            Program.QRconfig.txt_sizePost = textBox_set_post_size.Text;
            Program.QRconfig.txt_validation_query = textBox_txt_validation_query.Text;
            

            parameterDeclareDBF();
            ///////////////////////add new row!!!!
            fs = new FileStream(Program.QRconfig.xmlPath, FileMode.Create, FileAccess.Write);
            lq.Add(Program.QRconfig);
            xs.Serialize(fs, lq);
            ///////////close after read every time
            fs.Close();
        }


        private void label_state_cloud_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)

        {
                
                textBox2.Text = "     0.34 g/pcs"; //char(15)
                textBox3.Text = "      117 pcs"; //char(13)                                            
                textBox1.Focus();
                
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

     
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void xmlReadAndCreate_exist() ////////////warning infinit loop.
        {
            try
            {
                if (Program.QRconfig.xmlPath == null) { Program.QRconfig.xmlPath = xmlFile_path; }
                //string filePath = Path.GetDirectoryName(Program.QRconfig.xmlPath);
                FileStream fs = new FileStream(Program.QRconfig.xmlPath, FileMode.Open, FileAccess.ReadWrite);
                lq = (List<QRCode_config>)xs.Deserialize(fs);

                /////inherit parameter ref. 
                Program.QRconfig.macNo = lq[lq.Count - 1].macNo;
                Program.QRconfig.iconPath = lq[lq.Count - 1].iconPath.ToString();
                Program.QRconfig.level_QRCode = lq[lq.Count - 1].level_QRCode;
                Program.QRconfig.iconSize = lq[lq.Count - 1].iconSize;
                Program.QRconfig.interval_time = lq[lq.Count - 1].interval_time;
                Program.QRconfig.txtReadLinePath = lq[lq.Count - 1].txtReadLinePath;
                Program.QRconfig.xmlPath = lq[lq.Count - 1].xmlPath;
                Program.QRconfig.dbfPath = lq[lq.Count - 1].dbfPath;
                Program.QRconfig.txtLoggerPath = lq[lq.Count - 1].txtLoggerPath;
                Program.QRconfig.rangeTimeChr = lq[lq.Count - 1].rangeTimeChr;
                Program.QRconfig.syncSheetChr = lq[lq.Count - 1].syncSheetChr;
                Program.QRconfig.getSheetChr = lq[lq.Count - 1].getSheetChr;
                Program.QRconfig.syncRange = lq[lq.Count - 1].syncRange;
                Program.QRconfig.maximum_weight_g = lq[lq.Count - 1].maximum_weight_g;
                Program.QRconfig.minimum_weight_g = lq[lq.Count - 1].minimum_weight_g;
                Program.QRconfig.pictureSizeSlt = lq[lq.Count - 1].pictureSizeSlt;
                Program.QRconfig.txt_xPoint = lq[lq.Count - 1].txt_xPoint;
                Program.QRconfig.txt_yPoint = lq[lq.Count - 1].txt_yPoint;
                Program.QRconfig.txt_sizePost = lq[lq.Count - 1].txt_sizePost;
                Program.QRconfig.txt_validation_query = lq[lq.Count - 1].txt_validation_query;

                parameterDeclareDBF();

                /////display config current
                numericUpDown_macNo.Value = Program.QRconfig.macNo;
                textBox_iconPath.Text = Program.QRconfig.iconPath;
                comboBox_levelQR.Text = Program.QRconfig.level_QRCode;
                numericUpDown_iconSize.Value = Program.QRconfig.iconSize;
                numericUpDown_interval_time.Value = Program.QRconfig.interval_time;
                textBox_text_readLine.Text = Program.QRconfig.txtReadLinePath;
                textBox_xml_path.Text = Program.QRconfig.xmlPath;
                textBox_dbf_path.Text = Program.QRconfig.dbfPath;
                textBox_logger_path.Text = Program.QRconfig.txtLoggerPath;
                textBox_range_time.Text = Program.QRconfig.rangeTimeChr;
                textBox_sync_sheetChr.Text = Program.QRconfig.syncSheetChr;
                textBox_get_sheetChr.Text = Program.QRconfig.getSheetChr;
                numericUpDown_maximum_weight_g.Value = Program.QRconfig.maximum_weight_g;
                numericUpDown_minimum_weight_g.Value = Program.QRconfig.minimum_weight_g;
                textBox_pictureBox_size.Text = Program.QRconfig.pictureSizeSlt;
                textBox_set_post_x.Text = Program.QRconfig.txt_xPoint;
                textBox_set_post_y.Text = Program.QRconfig.txt_yPoint;
                textBox_set_post_size.Text = Program.QRconfig.txt_sizePost;
                textBox_txt_validation_query.Text = Program.QRconfig.txt_validation_query;

                //lq[lq.Count].interval_time;
                ///////////close after read every time
                fs.Close();

                label_state_prop.Text = "Ready";
                label_state_prop.BackColor = System.Drawing.Color.LimeGreen;
                label_state_prop.ForeColor = System.Drawing.Color.Black;

                label_range_weight_kg.Text = (numericUpDown_maximum_weight_g.Value / 1000).ToString("##0.000") + " - " + (numericUpDown_minimum_weight_g.Value / 1000).ToString("##0.000");
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.ToString());

                label_state_prop.Text = "error not exist : " + ex.ToString();
                label_state_prop.BackColor = System.Drawing.Color.Red;
                label_state_prop.ForeColor = System.Drawing.Color.White;

                if (ex.ToString().ToLower().IndexOf("filenotfound") > 0)
                {
                    xmlCreateAndWrite_exist(); xmlReadAndCreate_exist();
                }

                event_logger_detect(ex.Message.ToString());
            }

        }

#endregion xmlReadWrite
    }

}
