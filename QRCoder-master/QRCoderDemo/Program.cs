﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QRCoderDemo
{
    static class Program
    {
        // Create a DataSet and put both tables in it.
        public static DataTable cache_table_routine = new DataTable("cache_table_routine");
        public static String date_Str_last = "";/// for use clear state Routine sync
        public static Boolean working_Routine_bool = false;

        public static Boolean netWork_available_bool = false;
        public static String dbfPath = "";
        public static String dbfName = "";
        public static String txtLastLine = "";
        public static String txtLastSync = "";
        public static QRCode_config QRconfig = new QRCode_config();
        public static DBF_data DBF_data = new DBF_data();
        public static GS1_data GS1_data = new GS1_data();

        public static String newLine_result_txt = "";

        public static Boolean sync_network_time_protocol_bool = false;/// sync time location on internet.
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form_macros_print());
            //Application.Run(new Form1());
        }

    }
    
}
