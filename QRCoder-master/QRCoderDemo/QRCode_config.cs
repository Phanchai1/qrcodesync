﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace QRCoderDemo
{
    public class QRCode_config
    {

        public int macNo { get; set; }
        public string level_QRCode { get; set; }
        public string iconPath { get; set; }
        public int iconSize { get; set; }
        public int interval_time { get; set; }
        public string txtReadLinePath { get; set; }
        public string xmlPath { get; set; }
        public string dbfPath { get; set; }
        public string txtLoggerPath { get; set; }
        public string rangeTimeChr { get; set; }
        public string syncSheetChr { get; set; }
        public string syncRange { get; set; }
        public string getSheetChr { get; set; }
        public int maximum_weight_g { get; set; }
        public int minimum_weight_g { get; set; }
        public string pictureSizeSlt { get; set; }
        public string txt_xPoint { get; set; }
        public string txt_yPoint { get; set; }
        public string txt_sizePost { get; set; }
        public string txt_validation_query { get; set; }
 
    }

    public class DBF_data
    {
        public double OAStamp_Id { get; set; }
        public string Package_Id { get; set; }
        public string Company_Id { get; set; }
        public string Type_Str { get; set; }
        public string Weight_Str { get; set; }
        public string rMacNo_Str { get; set; }
        public string dStamp_Id { get; set; }
        public string QR_IMG_ID { get; set; }
    }
    public class GS1_data
    {
        public string Package_Id { get; set; }
        public string Company_Id { get; set; }
        public string Type_Str { get; set; }
        public string Weight_Str { get; set; }
        public string rMacNo_Str { get; set; }
        public string DateTime_Id { get; set; }
    }
}
