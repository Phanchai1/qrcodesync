﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using static Google.Apis.Sheets.v4.SheetsService;
using System.Text.Json;

namespace Sync_Sheet
{

    public class QRCode_config
    {
        public int macNo { get; set; }
        public string level_QRCode { get; set; }
        public string iconPath { get; set; }
        public int iconSize { get; set; }
        public int interval_time { get; set; }
        public string txtReadLinePath { get; set; }
        public string xmlPath { get; set; }
        public string dbfPath { get; set; }
        public string txtLoggerPath { get; set; }
        public string rangeTimeChr { get; set; }
        public string syncSheetChr { get; set; }
        public string syncRange { get; set; }
        public string getSheetChr { get; set; }
        public int maximum_weight_g { get; set; }
        public int minimum_weight_g { get; set; }
        public string pictureSizeSlt { get; set; }
        public string txt_xPoint { get; set; }
        public string txt_yPoint { get; set; }
        public string txt_sizePost { get; set; }
        public string txt_validation_query { get; set; }


    }
    public class DataSync
    {
        public string OASTAMP_ID { get; set; }
        public string PACKAGE_ID { get; set; }
        public string COMPANY_ID { get; set; }
        public string TYPE_STR { get; set; }
        public string WEIGHT_STR { get; set; }
        public string RMACNO_STR { get; set; }
        public string DSTAMP_ID { get; set; }
        public string QR_IMG_ID { get; set; }
    }

    internal class Program
    {
        static QRCode_config[] LoadConfig()
        {
            XmlSerializer xmlserializer = new System.Xml.Serialization.XmlSerializer(typeof(QRCode_config[]));
            //Use using so that streamReader object will be cleaned up properly after use. 
            using (StreamReader streamReader = new StreamReader(AppDomain.CurrentDomain.BaseDirectory+"\\XML_config\\qrcode_config.xml"))
            {
                return (QRCode_config[])xmlserializer.Deserialize(streamReader);
            }
        }
        static UserCredential GoogleAuthorization( QRCode_config config)
        {
            //Google Authorization 
            UserCredential credential;
            FileStream stream;
            try
            {
                stream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "gg_credential\\credentials.json", FileMode.Open, FileAccess.Read);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return null;
            }
            string[] Scopes = { SheetsService.Scope.Spreadsheets };
            string credPath = System.Environment.GetFolderPath(
                   System.Environment.SpecialFolder.Personal);
            credPath = Path.Combine(credPath, ".credentials/sheets.googleapis.com-dotnet-quickstart.json");

            Console.WriteLine(credPath);
            credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                   GoogleClientSecrets.Load(stream).Secrets,
                   Scopes,
                   "user",
                   CancellationToken.None,
                   new FileDataStore(credPath, true)).Result;
            //MessageBox.Show("Credential file saved to: " + credPath);
            Console.WriteLine("Credential file saved to: " + credPath);
            return credential;

        }
        static void SyncSheet(UserCredential credential,QRCode_config config , out List<FileInfo> success,out List<FileInfo> fail)
        {
            string ApplicationName = "QRCoderDemo25052019";
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            DataSync data = new DataSync();
            int columncount = data.GetType().GetProperties().Length;
            char columntext = (char)(64 + columncount);
            String range = config.syncRange + columntext;
            Console.WriteLine(config.syncSheetChr + " , " + range);

            IList<IList<object>> upload = new List<IList<object>>();

            DirectoryInfo SyncQueue = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "SyncQueue\\");
            FileInfo[] jsons = SyncQueue.GetFiles();

            success = new List<FileInfo>();
            fail = new List<FileInfo>();

            for (int i = 0; i < jsons.Length; i++)
            {
                Console.WriteLine(jsons[i].FullName);
                string json = File.ReadAllText(jsons[i].FullName);    
                DataSync jsonObject = JsonSerializer.Deserialize<DataSync>(json);

                List<object> v = new List<object>();
                bool markwrite = true;
               
                foreach (var item in jsonObject.GetType().GetProperties() )
                {
                    try
                    {
                        if (item.Name == "WEIGHT_STR")
                        {
                            if (item.GetValue(jsonObject).ToString().Split(')')[1] == "")
                            {
                                Console.BackgroundColor = ConsoleColor.DarkRed;
                                Console.WriteLine(jsons[i].Name);
                                Console.WriteLine("\t" + item.Name + ":\t\t" + item.GetValue(jsonObject));
                                Console.BackgroundColor = ConsoleColor.Black;
                                markwrite = false;
                            }
                        }
                    }catch(Exception e)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine(jsons[i].Name);
                        Console.WriteLine("\t" + e.Message);
                        Console.BackgroundColor = ConsoleColor.Black;
                        markwrite = false;
                    }
                    v.Add(item.GetValue(jsonObject).ToString());
                }

                if (markwrite)
                {
                    upload.Add(v);
                    success.Add(jsons[i]);
                }else
                {

                    fail.Add(jsons[i]);
                }
            }

            if (upload.Count > 0)
            {
                UpdatGoogleSheetinBatch(upload, config.syncSheetChr, range, service);
            }
        }

        private static void MoveFiles(string foldername , List<FileInfo> files)
        {
            foreach (var file in files)
            {
                string moveto = foldername + DateTime.Now.ToString("ddhhmmss") + "." + file.Name;
                File.Move(file.FullName, moveto);
            }
        }

        private static void UpdatGoogleSheetinBatch(IList<IList<Object>> values, string spreadsheetId, string newRange, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.AppendRequest request =
               service.Spreadsheets.Values.Append(new ValueRange() { Values = values }, spreadsheetId, newRange);
            request.InsertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            var response = request.Execute();
        }

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            var configs = LoadConfig();
            var config = configs[configs.Length - 1];
            var credential = GoogleAuthorization(config);
            var moveFolder = true;


            List<FileInfo> success = null;
            List<FileInfo> failure = null;
            try
            {
                SyncSheet(credential, config,out success,out failure);
            }
            catch
            {
                moveFolder = false;
            }
            if (moveFolder)
            {
                MoveFiles(AppDomain.CurrentDomain.BaseDirectory + "SyncSuccess\\",success);
                MoveFiles(AppDomain.CurrentDomain.BaseDirectory + "SyncError\\",failure);
                //   ErrorList();
            }
        }
    }
}










