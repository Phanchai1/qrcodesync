﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.



;DatatimeserverSync

;  full_command_line := DllCall("GetCommandLine", "str")
;  Run *RunAs "sync_time.bat" /restart , ,



;curl -X 'GET' \
;  'https://timeapi.io/api/Time/current/zone?timeZone=Asia/Bangkok' \
;  -H 'accept: application/json'

#include json.ahk

timeSync()
{
    synctime := false
    try{
      oWhr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
      oWhr.Open("GET", "https://timeapi.io/api/Time/current/zone?timeZone=Asia/Bangkok", false )
      oWhr.SetRequestHeader("Content-Type", "application/json")
      oWhr.SetRequestHeader("Authorization", "Bearer 80b44ea9c302237f9178a137d9e86deb-20083fb12d9579469f24afa80816066b")
      oWhr.Send()
      oWhr.WaitForResponse()
      synctime := true
    }Catch
    {
       synctime := false
    }
    

    if(synctime)
    {
      text := oWhr.ResponseText
      obj := Json.Load(text)
      h := obj.hour
      m := obj.minute
      s := obj.seconds
      mm := obj.month
      dd := obj.day
      yy := obj.year
      run, *RunAs %comspec% /c time %h%:%m%:%s% ,Hide
      run, *RunAs %comspec% /c date %mm%-%dd%-%yy%,Hide
    }
}

timeSync()
SetTimer timeSync,3600000




runSync()
{
  SoundBeep, 3000, 500
  full_command_line := DllCall("GetCommandLine", "str")
  Run *RunAs "Sync Sheet\\Sync Sheet\\bin\\Debug\\Sync Sheet.exe" /restart , ,
}
SetTimer runSync,30000



loop
{
  if WinExist("ThaiPET QRPrint")
    WinActivate ; Use the window found by WinExist.
}



F11::runSync()
F6::Pause